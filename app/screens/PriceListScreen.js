import React, { useEffect, useState, useRef } from "react";
import { Alert, Keyboard, StyleSheet, BackHandler, View } from "react-native";
import { Icon } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import AddButton from "../components/AddButton";
import firebase from "firebase";
import uuid from "react-native-uuid";
import OverlayAI from "../components/OverlayAI";

import PriceCard from "../components/PriceCard";
import TextInputCard from "../components/TextInputCard";
import routes from "../navigation/routes";

export default function PriceListScreen({ navigation, route }) {
  const scrollViewRef = useRef();

  const { area, priceList, id: areaID, onGoBack } = route.params;
  const [profession, onProfessionChange] = useState(area);
  const [currentPriceList, setCurrentPriceList] = useState([]);
  const [deleteItems, setDeleteItems] = useState([]);

  const [savePressed, setSavePressed] = useState(undefined);

  const [keyboardIsOpen, setKeyboardIsOpen] = useState(false);

  const [addPressed, setAddPressed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const docRef = firebase
    .firestore()
    .collection("users")
    .doc(firebase.auth().currentUser.uid)
    .collection("areas");

  useEffect(() => {
    const showSubscription = Keyboard.addListener("keyboardDidShow", () => {
      setKeyboardIsOpen(true);
    });
    const hideSubscription = Keyboard.addListener("keyboardDidHide", () => {
      setKeyboardIsOpen(false);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  useEffect(() => {
    let mounted = false;

    let dbPriceList = priceList.filter((item) => item.areaID === areaID);
    if (dbPriceList.length <= 0)
      dbPriceList.push({
        id: uuid.v1(),
        hours: 0,
        minutes: 0,
        price: "",
        procedure: "",
      });

    if (!mounted) setCurrentPriceList([...dbPriceList]);
    return () => (mounted = true);
  }, []);

  useEffect(() => {
    if (addPressed) {
      changePL();
      setAddPressed(false);
    }
  }, [currentPriceList]);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Icon
          name="check"
          type="feather"
          onPress={() => {
            saveNotes();
          }}
        />
      ),
    });
  }, [profession, currentPriceList]);

  useEffect(() => {
    if (savePressed && isDataCorrect()) updateDBArea();
  }, [profession, currentPriceList]);

  useEffect(() => {
    if (savePressed) {
      if (keyboardIsOpen) Keyboard.dismiss();
      else if (isDataCorrect()) updateDBArea();
    }
  }, [savePressed]);

  const moveToBottom = () => {
    scrollViewRef.current.scrollToEnd({ animated: true });
  };

  const onAddCard = () => {
    if (keyboardIsOpen) {
      setAddPressed(true);
      Keyboard.dismiss();
    } else {
      changePL();
    }
  };

  const onDeleteCard = (value) => {
    const items = currentPriceList.filter((item) => item.id !== value.id);
    if (items.length !== 0) {
      setCurrentPriceList(items);
      setDeleteItems([...deleteItems, value]);
    } else {
      Alert.alert("", "У вас должна быть хотя бы одна заполненная карточка");
    }
  };
  const changePL = () => {
    setCurrentPriceList([
      ...currentPriceList,
      { id: uuid.v1(), hours: 0, minutes: 0, price: "", procedure: "" },
    ]);
    setTimeout(moveToBottom, 200);
  };

  const checkIfAreCardsFilled = () => {
    let returnVal = true;
    for (let i = 0; i < currentPriceList.length; i++) {
      if (
        currentPriceList[i].procedure.trim() === "" ||
        isNaN(currentPriceList[i].price)
      ) {
        returnVal = false;
        break;
      }
    }
    return returnVal;
  };

  const checkIfDurationIsCorrect = () => {
    let returnVal = true;
    for (let i = 0; i < currentPriceList.length; i++) {
      if (
        currentPriceList[i].hours === 0 &&
        currentPriceList[i].minutes === 0
      ) {
        returnVal = false;
        break;
      }
    }
    return returnVal;
  };

  const updateCurrentPriceList = (value) => {
    setCurrentPriceList(
      currentPriceList.map((item) => (item.id === value.id ? value : item))
    );
  };

  const isDataCorrect = () => {
    if (checkIfAreCardsFilled() && checkIfDurationIsCorrect()) {
      return true;
    } else {
      if (!checkIfAreCardsFilled()) {
        Alert.alert(
          "",
          "Заполните карточки. Поля не должны быть пустыми и стоимость должна быть корректной"
        );
      } else if (!checkIfDurationIsCorrect())
        Alert.alert("", "Пожалуйста, заполните корректно время.");
      setSavePressed(false);
      return false;
    }
  };

  const updateDBArea = async () => {
    setIsLoading(true);
    await docRef.doc(areaID).set({
      area: profession,
    });

    updateDBPriceList();
    setIsLoading(false);

    onGoBack();
    navigation.goBack();
  };

  const updateDBPriceList = () => {
    const snapshot = docRef.doc(areaID).collection("price-list");
    currentPriceList.forEach(async (item) => {
      snapshot.doc(item.id).set({
        hours: item.hours,
        minutes: item.minutes,
        price: item.price,
        procedure: item.procedure,
      });
    });

    deleteItems.forEach(async (item) => {
      snapshot.doc(item.id).delete();
    });
  };

  const saveNotes = () => {
    return profession.length === 0
      ? Alert.alert("", "Какая ваша профессия?")
      : setSavePressed(true);
  };

  return (
    <>
      <ScrollView style={styles.container} ref={scrollViewRef}>
        <OverlayAI visible={isLoading} />
        <View>
          <TextInputCard
            label="Категория услуг (например, Маникюр, Ресницы, Брови)"
            value={profession}
            onChangeText={(text) => onProfessionChange(text)}
            onBlur={() => {
              if (addPressed) {
                changePL();
                setAddPressed(false);
              }
              if (savePressed && isDataCorrect()) updateDBArea();
            }}
          />
        </View>
        <View style={styles.priceListContainer}>
          {currentPriceList.map((item) => (
            <PriceCard
              key={item.id}
              onChangeState={(value) => updateCurrentPriceList(value)}
              value={item}
              deleteCard={(value) => onDeleteCard(value)}
            />
          ))}
        </View>
      </ScrollView>

      <AddButton onPress={() => onAddCard()} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
  },
  priceListContainer: {
    marginBottom: 20,
  },
});
