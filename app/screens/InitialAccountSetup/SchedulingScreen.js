import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  ActivityIndicator,
  View,
  FlatList,
  Modal,
  Pressable,
  Alert,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

import { Button, Image, ListItem, Text } from "react-native-elements";
import DateTimePickerResult from "@react-native-community/datetimepicker";
import dayjs from "dayjs";
import firebase from "firebase";
import { Picker } from "@react-native-picker/picker";

import defaultStyle from "../../config/styles";
import RegisterExecutionLines from "../../components/RegisterExecutionLines";
import routes from "../../navigation/routes";
import scheduleArray from "../../config/schedule";
import UnderDivider from "../../components/UnderDivider";
import ProjectActivityIndicator from "../../components/ProjectActivityIndicator";
import TimePicker from "../../components/scheduling/TimePicker";
import TimeSelectCard from "../../components/scheduling/TimeSelectCard";

export default function SchedulingScreen({ navigation, route }) {
  const [modalVisible, setModalVisible] = useState(false);

  const [isLoaded, setIsLoaded] = useState(false);
  const [schedule, setSchedule] = useState([]);
  const [refresh, setRefresh] = useState(true);
  const [isDisable, setIsDisable] = useState(true);
  const [startTime, setStartTime] = useState(new Date());

  const [dayName, setDayName] = useState("");
  const [dayID, setDayID] = useState();

  const scheduleRef = firebase
    .firestore()
    .collection("users")
    .doc(firebase.auth().currentUser.uid)
    .collection("schedule");

  useEffect(() => {
    let isSubscribed = true;
    scheduleRef.get().then((snapshot) => {
      if (snapshot.docs.length <= 0) {
        scheduleArray.forEach((doc) => {
          scheduleRef.add(doc);
        });
      }
      let scheduleData = [];

      scheduleRef
        .orderBy("dayID", "asc")
        .get()
        .then((snapshot) => {
          const data = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));
          data.forEach((s) => scheduleData.push(s));

          if (isSubscribed) {
            setSchedule(scheduleData);
            setIsLoaded(true);
          }
        });
    });

    return () => (isSubscribed = false);
  }, []);

  useEffect(() => {
    if (schedule.filter((s) => s.subtitle !== "Выходной").length > 0) {
      setIsDisable(false);
    } else {
      setIsDisable(true);
    }
  }, [schedule]);

  const handleUpdateDatabaseSchedule = (id) => {
    const updateObject = schedule.find((s) => s.dayID === id);
    scheduleRef
      .doc(updateObject.id)
      .update({
        subtitle: updateObject.subtitle,
        startWorkTime: updateObject.startWorkTime,
        endWorkTime: updateObject.endWorkTime,
        startLunchTime: updateObject.startLunchTime,
        endLunchTime: updateObject.endLunchTime,
        buttonTitle: updateObject.buttonTitle,
        lunchSubtitle: updateObject.lunchSubtitle,
      })
      .catch(() => {
        Alert.alert(
          "Не смогли сохранить...",
          "Извините, мы не смогли сохранить ваши данные"
        );
      });
  };

  const handleChangeLocalSchedule = (
    dayID,
    eraseData = false,
    startTime = undefined,
    endTime = undefined,
    startLunchTime,
    endLunchTime
  ) => {
    const scheduleData = [...schedule];
    const index = scheduleData.map((item) => item.dayID).indexOf(dayID);

    if (startLunchTime && endLunchTime) {
      scheduleData[index].startLunchTime = new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate(),
        startLunchTime.split(":")[0],
        startLunchTime.split(":")[1]
      );
      scheduleData[index].endLunchTime = new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate(),
        endLunchTime.split(":")[0],
        endLunchTime.split(":")[1]
      );
    } else {
      scheduleData[index].startLunchTime = null;
      scheduleData[index].endLunchTime = null;
    }

    if (startTime && endTime) {
      scheduleData[index].startWorkTime = new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate(),
        startTime.split(":")[0],
        startTime.split(":")[1]
      );
      scheduleData[index].endWorkTime = new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate(),
        endTime.split(":")[0],
        endTime.split(":")[1]
      );
    } else {
      scheduleData[index].startWorkTime = null;
      scheduleData[index].endWorkTime = null;
    }

    if (!eraseData) {
      scheduleData[index].subtitle = `Рабочий день: ${startTime} – ${endTime}`;
      scheduleData[index].buttonTitle = "Стереть";
      if (startLunchTime && endLunchTime)
        scheduleData[
          index
        ].lunchSubtitle = `Обеденный перерыв: ${startLunchTime} – ${endLunchTime}`;
      else scheduleData[index].lunchSubtitle = "Обеденный перерыв: нет";
    } else {
      scheduleData[index].subtitle = "Выходной";
      scheduleData[index].buttonTitle = "Добавить";
      scheduleData[index].lunchSubtitle = "";
    }
    setSchedule(scheduleData);
    handleUpdateDatabaseSchedule(dayID);
    setRefresh(!refresh);
  };

  return (
    <View style={[styles.container, modalVisible ? { opacity: 0.6 } : ""]}>
      <Image
        source={require("../../assets/images/female-hands.png")}
        style={styles.image}
        PlaceholderContent={<ActivityIndicator size="large" />}
      />
      <View style={styles.content}>
        {!route.params && <RegisterExecutionLines step={1} />}
        <Text style={[defaultStyle.mainText, styles.text]}>
          Часы рабочей недели
        </Text>
        {!isLoaded ? (
          <ProjectActivityIndicator />
        ) : (
          <FlatList
            style={{ flex: 1 }}
            data={schedule}
            keyExtractor={(item) => item.dayID.toString()}
            renderItem={({ item }) => (
              <ListItem bottomDivider>
                <ListItem.Content>
                  <ListItem.Title>{item.day}</ListItem.Title>
                  <ListItem.Subtitle>{item.subtitle}</ListItem.Subtitle>
                  {item.lunchSubtitle ? (
                    <ListItem.Subtitle>{item.lunchSubtitle}</ListItem.Subtitle>
                  ) : null}
                </ListItem.Content>
                <Button
                  title={item.buttonTitle}
                  type="clear"
                  titleStyle={styles.button}
                  onPress={() => {
                    setDayID(item.dayID);
                    if (item.buttonTitle === "Добавить") {
                      setModalVisible(!modalVisible);
                      setDayName(item.day);
                    } else {
                      handleChangeLocalSchedule(item.dayID, true);
                    }
                  }}
                />
              </ListItem>
            )}
            extraData={refresh}
          />
        )}
      </View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <TimeSelectCard
          onPress={(
            startWorkTime = undefined,
            endWorkTime = undefined,
            startLunchTime,
            endLunchTime
          ) => {
            if (startWorkTime && endWorkTime) {
              handleChangeLocalSchedule(
                dayID,
                false,
                startWorkTime,
                endWorkTime,
                startLunchTime,
                endLunchTime
              );
            }
            setModalVisible(!modalVisible);
          }}
          dayName={dayName}
        />
      </Modal>
      {!route.params && (
        <UnderDivider
          disabled={isDisable}
          buttonTitle="Готово"
          onPress={() => navigation.navigate(routes.CHOOSE_AREA_WORK)}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: defaultStyle.colors.light,
  },
  content: {
    flex: 1,
    padding: 15,
  },
  button: {
    color: defaultStyle.colors.medium,
    textTransform: "uppercase",
    letterSpacing: 1.3,
    fontSize: 14,
  },
  image: {
    height: 250,
    resizeMode: "cover",
  },
  text: {
    fontSize: 28,
    marginBottom: 20,
  },
});
