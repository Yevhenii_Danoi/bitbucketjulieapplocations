import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  ActivityIndicator,
  View,
  Pressable,
  Alert,
} from "react-native";
import { Image, Text, Icon } from "react-native-elements";
import firebase from "firebase";
import uuid from "react-native-uuid";
import AsyncStorage from "@react-native-async-storage/async-storage";

import defaultStyle from "../../config/styles";
import UnderDivider from "../../components/UnderDivider";
import RegisterExecutionLines from "../../components/RegisterExecutionLines";
import routes from "../../navigation/routes";
import ProjectActivityIndicator from "../../components/ProjectActivityIndicator";
import { ScrollView } from "react-native-gesture-handler";
import AddButton from "../../components/AddButton";

function useForceUpdate() {
  const [value, setValue] = useState([]);
  return () => setValue((area) => value + 1);
}

export default function ChoiceAreasWork({ navigation, route }) {
  if (route.params && route.params.fromSettings)
    var goToScreen = routes.SETTINGS_SCREEN;

  const docRef = firebase
    .firestore()
    .collection("users")
    .doc(firebase.auth().currentUser.uid)
    .collection("areas");

  const [areas, setAreas] = useState([]);
  const [priceList, setPriceList] = useState([]);

  const [isDisable, setIsDisable] = useState(true);
  const [isLoaded, setIsLoaded] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(null);

  const collectIdsAndDocs = (doc) => {
    return {
      id: doc.id,
      ...doc.data(),
    };
  };

  useEffect(() => {
    let mounted = false;
    if (!mounted) updateLocalAreas();

    return () => (mounted = true);
  }, []);

  const deleteArea = (areaId) => {
    Alert.alert("", "Вы действительно хотите удалить направление?", [
      {
        text: "Нет",
        style: "cancel",
      },
      {
        text: "Да",
        onPress: async () => {
          await docRef
            .doc(areaId)
            .collection("price-list")
            .get()
            .then((snapshot) => {
              snapshot.forEach((doc) => {
                doc.ref.delete();
              });
            })
            .then(() => {
              docRef.doc(areaId).delete();
            });
        },
      },
    ]);
  };

  const updateLocalAreas = () => {
    try {
      docRef.onSnapshot((snapshot) => {
        const myAreas = snapshot.docs.map(collectIdsAndDocs);
        setAreas([...myAreas]);
        updateLocalPriceList([...myAreas]);
        setIsLoaded(true);
      });
    } catch (error) {
      console.log(error);
    }
  };

  const updateLocalPriceList = (areas) => {
    let priceListData = new Map();
    areas.map(
      async (area) =>
        await docRef
          .doc(area.id)
          .collection("price-list")
          .get()
          .then((snapshot) => {
            const data = snapshot.docs.map((doc) => ({
              id: doc.id,
              areaID: area.id,
              ...doc.data(),
            }));
            data.forEach((s) => {
              if (!priceListData.has(s.id)) {
                priceListData.set(s.id, s);
              }
            });
            setPriceList(Array.from(priceListData, ([name, value]) => value));
            if (priceListData.size > 0) setIsDisable(false);
          })
    );
  };

  return (
    <View style={styles.container}>
      <Image
        source={require("../../assets/images/female-eye.png")}
        style={styles.image}
        PlaceholderContent={<ActivityIndicator />}
      />
      <View style={styles.content}>
        {!goToScreen && <RegisterExecutionLines step={2} />}
        <Text style={[defaultStyle.mainText, styles.mainText]}>
          Определите свое направление
        </Text>

        {!isLoaded ? (
          <ProjectActivityIndicator />
        ) : areas.length <= 0 ? (
          <View style={styles.hintTextContainer}>
            <Text style={styles.hintText}>
              Потратье пару минут, чтобы создать собственный прайс-лист
            </Text>
          </View>
        ) : (
          <ScrollView style={{ flex: 1 }}>
            {areas.map(({ id, area }, index) => {
              return (
                <Pressable
                  key={id}
                  onPress={() => {
                    setCurrentIndex(index === currentIndex ? null : index);
                  }}
                  onLongPress={() => deleteArea(id)}
                  style={styles.cardContainer}
                >
                  <View style={styles.card}>
                    <View style={styles.headingContainer}>
                      <Text style={styles.heading}>{area}</Text>
                      <Icon
                        name="edit"
                        type="material-icon"
                        onPress={() =>
                          navigation.navigate(routes.PRICE_LIST_SCREEN, {
                            id,
                            area,
                            priceList,
                            onGoBack: () => updateLocalAreas(),
                          })
                        }
                      />
                    </View>
                    {index === currentIndex && (
                      <View style={styles.priceListContainer}>
                        {priceList.map(
                          (item) =>
                            item.areaID === id && (
                              <View
                                style={styles.procedureContainer}
                                key={item.id}
                              >
                                <Text style={styles.body}>
                                  {item.procedure}
                                </Text>
                                <Text style={styles.body}>
                                  {item.price} грн.
                                </Text>
                              </View>
                            )
                        )}
                      </View>
                    )}
                  </View>
                </Pressable>
              );
            })}
          </ScrollView>
        )}
        <AddButton
          style={styles.addPriceList}
          onPress={() =>
            navigation.navigate(routes.PRICE_LIST_SCREEN, {
              id: uuid.v1(),
              area: "",
              priceList: [],
              onGoBack: () => {
                updateLocalAreas();
              },
            })
          }
        />
      </View>
      {!goToScreen && (
        <UnderDivider
          disabled={isDisable}
          buttonTitle="Готово"
          onPress={() => navigation.navigate(routes.ADD_AVATAR)}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  addPriceList: {
    // flex: 1,
    marginTop: 5,
    alignItems: "center",
    justifyContent: "flex-end",
  },
  body: {
    fontSize: 15,
  },
  cardContainer: {
    flexGrow: 1,
    backgroundColor: defaultStyle.colors.white,
    padding: 15,
    marginBottom: 10,
  },
  card: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: defaultStyle.colors.light,
  },
  content: {
    flex: 1,
    padding: 15,
  },
  heading: {
    fontSize: 18,
    fontWeight: "900",
    textTransform: "uppercase",
    letterSpacing: 0.9,
  },
  headingContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  hintText: {
    color: defaultStyle.colors.medium,
    fontSize: 16,
    textAlign: "center",
    lineHeight: 30,
    margin: 10,
    paddingHorizontal: 15,
  },
  hintTextContainer: {
    alignItems: "center",
  },
  image: {
    height: 250,
    resizeMode: "cover",
  },
  mainText: {
    fontSize: 22,
    marginBottom: 20,
  },
  priceListContainer: {
    borderTopColor: defaultStyle.colors.secondary,
    borderTopWidth: 3,
    marginTop: 10,
    paddingTop: 10,
  },
  procedureContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
