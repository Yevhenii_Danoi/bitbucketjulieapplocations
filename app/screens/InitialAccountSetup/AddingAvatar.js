import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image, Text, Alert } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { ThemeProvider, Button, Avatar } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import firebase from "firebase";

import { getCapitalLetters } from "../../config/utils";
import defaultStyle from "../../config/styles";
import UnderDivider from "../../components/UnderDivider";
import {
  GestureHandlerRootView,
  TouchableOpacity,
} from "react-native-gesture-handler";
import OverlayAI from "../../components/OverlayAI";

export default function AddingAvatar() {
  const [image, setImage] = useState(null);
  const [title, setTitle] = useState(null);
  const [isSaving, setIsSaving] = useState(false);

  useEffect(() => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .get()
      .then((snapshot) => {
        setTitle(snapshot.data().name + " " + snapshot.data().surname);
      });
  }, []);
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const handleCompleteConfiguration = async () => {
    setIsSaving(true);
    if (image) {
      const response = await fetch(image);
      const blob = await response.blob();

      const ref = firebase
        .storage()
        .ref()
        .child("users/" + firebase.auth().currentUser.uid + "/profilePhoto");

      ref
        .put(blob)
        .then(() => {
          ref.getDownloadURL().then((result) => {
            firebase
              .firestore()
              .collection("users")
              .doc(firebase.auth().currentUser.uid)
              .update({
                isCompletedConfiguration: true,
                profilePhoto: result,
              });
          });
        })
        .catch(() => {
          Alert.alert(
            "Не удалось загрузить фото профиля",
            "Проверьте Интернет-соединение"
          );
          setIsSaving(false);
        });
    } else {
      firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .update({
          isCompletedConfiguration: true,
          profilePhoto: null,
        });
    }
  };

  return (
    <ThemeProvider theme={defaultStyle.theme}>
      <OverlayAI visible={isSaving} />
      <SafeAreaView style={styles.container}>
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Text style={styles.text}>
            Загрузите свое фото, чтобы клиенты вас узнавали (по желанию)
          </Text>
        </View>
        <GestureHandlerRootView style={{ flex: 2 }}>
          <TouchableOpacity activeOpacity={0.8} onPress={pickImage}>
            {image ? (
              <Avatar size={200} rounded source={{ uri: image }}>
                <Avatar.Accessory size={44} />
              </Avatar>
            ) : (
              <Avatar
                size={200}
                rounded
                title={title ? getCapitalLetters(title) : ""}
                icon={{ name: "backup", type: "material" }}
                containerStyle={{
                  backgroundColor: "rgba(0,0,0,0.3)",
                }}
              >
                <Avatar.Accessory size={44} />
              </Avatar>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            style={{ alignItems: "center", marginTop: 20 }}
            onPress={() => setImage(null)}
          >
            {image && (
              <Text
                style={{ fontSize: 20, color: defaultStyle.colors.primary }}
              >
                Удалить фото
              </Text>
            )}
          </TouchableOpacity>
        </GestureHandlerRootView>
      </SafeAreaView>
      <UnderDivider
        buttonTitle="Готово"
        onPress={() => handleCompleteConfiguration()}
      />
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
  },
  text: {
    fontSize: 22,
    textAlign: "center",
    color: defaultStyle.colors.lightblack,
  },
});
