import React, { useEffect } from "react";
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Dimensions,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { Button, ThemeProvider } from "react-native-elements";
import defaultStyle from "../config/styles";
import routes from "../navigation/routes";
import useAuth from "../auth/useAuth";

export default function WelcomeScreen({ navigation }) {
  const { setError } = useAuth();
  useEffect(() => {
    AsyncStorage.setItem("modalOnFirstLaunchShown", JSON.stringify(false));
  }, []);
  return (
    <ImageBackground
      source={require("../assets/images/main-background.jpg")}
      style={styles.container}
      resizeMode="cover"
    >
      <View style={styles.darkBlur}></View>
      <Text style={styles.h1}>Julie</Text>
      <Text style={styles.text}>
        Экономьте на записи, чтобы сосредоточиться на работе
      </Text>
      <ThemeProvider theme={defaultStyle.theme}>
        <Button
          buttonStyle={styles.buttonStyle}
          title="Зарегистрироваться"
          onPress={() => navigation.navigate(routes.CHOOSE_EMAIL)}
        />
        <Button
          title="Войти"
          buttonStyle={styles.buttonStyle}
          onPress={() => {
            navigation.navigate(routes.LOGIN);
            setError("");
          }}
        />
      </ThemeProvider>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  buttonStyle: {
    borderColor: "#fff",
    borderWidth: 1,
  },
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  darkBlur: {
    position: "absolute",
    top: 0,
    left: 0,
    backgroundColor: "#fff",
    opacity: 0,
    height: Dimensions.get("screen").height,
    width: Dimensions.get("screen").width,
  },
  h1: {
    fontSize: 40,
    fontWeight: "700",
    color: "#fff",
    marginBottom: 25,
    textShadowColor: "#333",
    textShadowRadius: 10,
  },
  text: {
    color: "#fff",
    fontSize: 24,
    fontWeight: "600",
    letterSpacing: 1.1,
    marginBottom: 40,
    paddingHorizontal: 20,
    textAlign: "center",
    textShadowColor: "#333",
    textShadowRadius: 10,
  },
});
