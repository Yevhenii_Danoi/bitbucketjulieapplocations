import React, { useEffect, useState } from "react";
import { Alert, Pressable, StyleSheet, Text, View } from "react-native";
import * as Yup from "yup";
import firebase from "firebase";
import {
  GestureHandlerRootView,
  TouchableOpacity,
} from "react-native-gesture-handler";

import * as ImagePicker from "expo-image-picker";
import { ErrorMessage } from "../../components/forms";
import { Avatar, Icon } from "react-native-elements";
import routes from "../../navigation/routes";
import useAuth from "../../auth/useAuth";
import Screen from "../../components/Screen";
import TextInput from "../../components/TextInput";
import { getCurrentPassword } from "../../config/constants";
import OverlayAI from "../../components/OverlayAI";
import { getCapitalLetters } from "../../config/utils";
import defaultStyle from "../../config/styles";

export default function AccountDetailsScreen({ navigation }) {
  const { updateEmail, error } = useAuth();
  const [isLoading, setIsLoading] = useState(true);
  const [image, setImage] = useState(null);
  const [isChangingPhoto, setIsChangingPhoto] = useState(false);

  const [currentUserEmail, setCurrentUserEmail] = useState(
    firebase.auth().currentUser.email
  );
  const [currentUserName, setCurrentUserName] = useState("");
  const [smsReminder, setSmsReminder] = useState("");

  const [currentNick, setCurrentNick] = useState("");
  const [oldUserName, setOldUserName] = useState("");

  useEffect(() => {
    let mounted = false;

    const fetchData = async () => {
      try {
        await firebase
          .firestore()
          .collection("users")
          .doc(firebase.auth().currentUser.uid)
          .get()
          .then((doc) => {
            if (!mounted) {
              setCurrentUserName(`${doc.data().name} ${doc.data().surname}`);
              setOldUserName(`${doc.data().name} ${doc.data().surname}`);
              setSmsReminder(`${doc.data().smsReminder}`);
              setCurrentNick(`${doc.data().nick}`);
              setImage(doc.data().profilePhoto);
              setIsLoading(false);
            }
          });
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();

    return () => (mounted = true);
  }, []);

  const handleUpdateUsername = () => {
    let name, surname;

    if (currentUserName.indexOf(" ") > -1) {
      name = currentUserName.substring(0, currentUserName.indexOf(" "));
      surname = currentUserName.substring(currentUserName.indexOf(" ") + 1);
    } else {
      name = currentUserName;
      surname = "";
    }
    const newSmsReminder = smsReminder.replace(
      oldUserName.trim(),
      currentUserName.trim()
    );

    if (name.length !== 0) {
      firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .update({
          name: name,
          surname: surname,
          smsReminder: newSmsReminder,
        })
        .then()
        .catch((error) => console.log(error));
    }
  };

  const handleUpdateEmail = () => {
    getCurrentPassword().then((currentPassword) => {
      const credential = firebase.auth.EmailAuthProvider.credential(
        firebase.auth().currentUser.email,
        currentPassword
      );
      firebase
        .auth()
        .currentUser.reauthenticateWithCredential(credential)
        .then(() => {
          updateEmail(currentUserEmail);
        })
        .then(
          firebase
            .firestore()
            .collection("users")
            .doc(firebase.auth().currentUser.uid)
            .update({
              email: currentUserEmail,
            })
        )
        .catch((error) => {
          console.log(error.message);
        });
    });
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setIsChangingPhoto(true);

      const response = await fetch(result.uri);
      const blob = await response.blob();

      const ref = firebase
        .storage()
        .ref()
        .child("users/" + firebase.auth().currentUser.uid + "/profilePhoto");

      ref
        .put(blob)
        .then(() => {
          ref.getDownloadURL().then((downloadURL) => {
            firebase
              .firestore()
              .collection("users")
              .doc(firebase.auth().currentUser.uid)
              .update({
                profilePhoto: downloadURL,
              })
              .then(() => {
                setImage(result.uri);
                setIsChangingPhoto(false);
              });
          });
        })
        .catch(() => {
          Alert.alert("Не удалось загрузить фото профиля");
        });
    }
  };

  const deletePhoto = () => {
    Alert.alert(
      "Удалить фото профиля?",
      "",
      [
        {
          text: "Отмена",
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            firebase
              .firestore()
              .collection("users")
              .doc(firebase.auth().currentUser.uid)
              .update({
                profilePhoto: null,
              })
              .then(() => {
                setImage(null);
              });
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <Screen style={styles.container}>
      <OverlayAI visible={isLoading} />
      <ErrorMessage error={error} visible={true} />
      <GestureHandlerRootView
        style={{ alignItems: "center", marginBottom: 20 }}
      >
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={pickImage}
          style={{ marginBottom: 15 }}
        >
          {image ? (
            <Avatar size={100} rounded source={{ uri: image }}>
              <Avatar.Accessory size={23} />
            </Avatar>
          ) : (
            <Avatar
              size={100}
              rounded
              title={getCapitalLetters(currentUserName)}
              containerStyle={{
                backgroundColor: "rgba(0,0,0,0.3)",
              }}
              titleStyle={{ fontSize: 28 }}
            >
              <Avatar.Accessory size={23} />
            </Avatar>
          )}
        </TouchableOpacity>
        {isChangingPhoto && <Text>Обновление...</Text>}
        <TouchableOpacity onPress={() => deletePhoto()}>
          {image && !isChangingPhoto && <Text>Удалить фото</Text>}
        </TouchableOpacity>
      </GestureHandlerRootView>

      <TextInput
        name="name"
        value={currentUserName}
        label="Имя"
        onChangeText={(fullname) => setCurrentUserName(fullname)}
        onBlur={() => handleUpdateUsername()}
      />
      <TextInput
        autoCapitalize="none"
        name="email"
        value={currentUserEmail}
        label="Email"
        onChangeText={(email) => setCurrentUserEmail(email)}
        onBlur={() => handleUpdateEmail()}
      />

      <Pressable onPress={() => navigation.navigate(routes.CHANGE_PASSWORD)}>
        <View style={styles.itemContainer}>
          <Icon name="lock" type="font-awesome" style={styles.icon} />
          <Text style={styles.text}>Изменить пароль</Text>
        </View>
      </Pressable>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  itemContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 15,
  },
  icon: {
    marginRight: 40,
  },
  text: {
    fontSize: 18,
    letterSpacing: 0.4,
  },
});
