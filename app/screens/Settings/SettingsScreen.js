import React from "react";
import { StyleSheet, Text, View, Pressable } from "react-native";
import {
  GestureHandlerRootView,
  TouchableOpacity,
} from "react-native-gesture-handler";

import { Icon } from "react-native-elements";

import useAuth from "../../auth/useAuth";
import routes from "../../navigation/routes";
import Screen from "../../components/Screen";

export default function SettingsScreen({ navigation }) {
  const { logout } = useAuth();

  return (
    <Screen style={styles.container}>
      <Pressable onPress={() => navigation.navigate(routes.ACCOUNT_DETAILS)}>
        <View style={styles.iconTextContainer}>
          <Icon
            type="material-community"
            name="account"
            size={30}
            style={styles.icon}
          />
          <Text style={styles.text}>Подробности аккаунта</Text>
        </View>
      </Pressable>
      <Pressable
        onPress={() => navigation.navigate(routes.PROFILE_SETTINGS_STACK)}
      >
        <View style={styles.iconTextContainer}>
          <Icon
            type="ionicon"
            name="settings-outline"
            size={30}
            style={styles.icon}
          />
          <Text style={styles.text}>Настройки профиля</Text>
        </View>
      </Pressable>

      <GestureHandlerRootView>
        <TouchableOpacity onPress={() => logout()}>
          <Text style={styles.text}>Выйти</Text>
        </TouchableOpacity>
      </GestureHandlerRootView>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  divider: {
    marginBottom: 15,
  },
  iconTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 15,
  },
  icon: {
    marginRight: 10,
  },
  text: {
    fontSize: 18,
    letterSpacing: 0.4,
  },
});
