import React, { useState, useEffect } from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";
import { Icon, Input } from "react-native-elements";
import * as Clipboard from "expo-clipboard";
import firebase from "firebase";

import routes from "../../navigation/routes";
import defaultStyle from "../../config/styles";
import UnderDivider from "../../components/UnderDivider";
import OverlayAI from "../../components/OverlayAI";

export default function ProfileSettingsScreen({ navigation }) {
  const [showTextCopiedNotification, setShowTextCopiedNotification] =
    useState("");
  const [smsReminder, setSmsReminder] = useState("");
  const [invitationLink, setInvitationLink] = useState("");
  const [isLoaded, setIsLoaded] = useState(true);

  const copyToClipboard = () => {
    Clipboard.setString(invitationLink);
    setShowTextCopiedNotification("Ссылка скопирована");
  };

  useEffect(() => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .get()
      .then((doc) => {
        setSmsReminder(doc.data().smsReminder);
        setInvitationLink(doc.data().recordingUrl);
      })
      .then(() => {
        setIsLoaded(false);
      });
  }, []);

  useEffect(() => {
    const timeoutHandle = setTimeout(() => {
      setShowTextCopiedNotification("");
    }, 2000);

    return () => {
      clearTimeout(timeoutHandle);
    };
  }, [showTextCopiedNotification]);

  const updateDatabase = () => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .update({
        smsReminder: smsReminder,
      })
      .then(navigation.goBack());
  };
  return (
    <View style={styles.container}>
      <OverlayAI visible={isLoaded} />
      <View style={styles.contentContainer}>
        <View>
          <Pressable
            onPress={() =>
              navigation.navigate(routes.SCHEDULING, { fromSettings: true })
            }
          >
            <View style={styles.iconTextContainer}>
              <Icon
                type="font-awesome"
                name="list-alt"
                size={26}
                style={styles.icon}
              />
              <Text style={styles.text}>Расписание</Text>
            </View>
          </Pressable>
          <Pressable
            onPress={() =>
              navigation.navigate(routes.CHOOSE_AREA_WORK, {
                fromSettings: true,
              })
            }
          >
            <View style={styles.iconTextContainer}>
              <Icon type="octicon" name="note" size={30} style={styles.icon} />
              <Text style={styles.text}>Услуги</Text>
            </View>
          </Pressable>
        </View>
        <Input
          rightIcon={
            <Icon
              type="material-community"
              name="content-copy"
              onPress={copyToClipboard}
            />
          }
          value={invitationLink}
          label="Ссылка для записи клиента"
          labelStyle={styles.labelStyle}
          containerStyle={styles.containerStyle}
          inputContainerStyle={styles.inputContainerStyle}
          textContentType="URL"
        />
        <Text style={styles.copiedText}>{showTextCopiedNotification}</Text>

        <Input
          autoCorrect={false}
          label="Смс-напоминание"
          value={smsReminder}
          onChangeText={(text) => setSmsReminder(text)}
          labelStyle={styles.labelStyle}
          containerStyle={styles.containerStyle}
          inputContainerStyle={[
            styles.inputContainerStyle,
            styles.messageContainer,
          ]}
          placeholder="Сообщение..."
          multiline={true}
        />
        <Text style={{ color: "#D22B2B" }}>
          Пожалуйста, не меняйте значение в скобках
        </Text>
      </View>
      <UnderDivider buttonTitle="Сохранить" onPress={updateDatabase} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
  },
  containerStyle: {
    paddingHorizontal: 0,
  },
  contentContainer: {
    padding: 15,
  },
  copiedText: {
    color: defaultStyle.colors.secondary,
  },
  icon: {
    marginRight: 10,
  },
  inputContainerStyle: {
    borderBottomWidth: 1,
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: -20,
  },
  iconTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 15,
  },
  labelStyle: {
    marginBottom: 5,
  },
  messageContainer: {
    height: 200,
    alignItems: "flex-start",
    paddingVertical: 10,
  },
  text: {
    fontSize: 18,
    letterSpacing: 0.4,
  },
});
