import React, { useState } from "react";
import { Pressable, StyleSheet, Text } from "react-native";
import { Icon } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import * as Yup from "yup";
import firebase from "firebase";

import { ErrorMessage, Form, FormField } from "../../components/forms";
import defaultStyle from "../../config/styles";
import routes from "../../navigation/routes";
import useAuth from "../../auth/useAuth";
import OverlayAI from "../../components/OverlayAI";
import RegisterScreen from "../../components/RegisterScreen";

const passwordRestrictionText =
  "8-30 символов, Одна заглавная, Одна прописная, Одна цифра";

const validationSchema = Yup.object().shape({
  currentPassword: Yup.string()
    .required("Это поле обязательно")
    .label("Пароль"),
  newPassword: Yup.string()
    .required("Это поле обязательно")
    .label("Пароль")
    .matches(
      /\w*[a-z]\w*/,
      passwordRestrictionText //маленькая буква
    )
    .matches(
      /\w*[A-Z]\w*/,
      passwordRestrictionText //большая буква
    )
    .matches(/\d/, passwordRestrictionText) //цифра
    .min(8, passwordRestrictionText)
    .max(30, passwordRestrictionText),
});

export default function ChangePasswordScreen({ navigation }) {
  const { updatePassword, isLoading, setError, error } = useAuth();
  const [visibleCurrPass, setVisibleCurrPass] = useState(true);
  const [visibleNewPass, setVisibleNewPass] = useState(true);

  const changePassword = ({ currentPassword, newPassword }) => {
    const credential = firebase.auth.EmailAuthProvider.credential(
      firebase.auth().currentUser.email,
      currentPassword
    );

    firebase
      .auth()
      .currentUser.reauthenticateWithCredential(credential)
      .then(() => {
        updatePassword(newPassword);
        navigation.goBack();
      })
      .catch((error) => {
        setError(error.message);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <OverlayAI visible={isLoading} />
      <Form
        initialValues={{ currentPassword: "", newPassword: "" }}
        onSubmit={(values) => changePassword(values)}
        validationSchema={validationSchema}
      >
        <RegisterScreen buttonTitle="Готово">
          <Pressable
            onPress={() => navigation.navigate(routes.ACCOUNT_DETAILS)}
          >
            <Icon type="ionicons" name="close" size={30} style={styles.icon} />
          </Pressable>
          <Text style={defaultStyle.mainText}>Изменить пароль</Text>
          <ErrorMessage error={error} visible={true} />
          <FormField
            autoCorrect={false}
            autoCapitalize="none"
            name="currentPassword"
            label="Текущий пароль"
            rightIcon={
              <Icon
                name={visibleCurrPass ? "eye-off-sharp" : "eye-sharp"}
                type="ionicon"
                onPress={() => {
                  setVisibleCurrPass(!visibleCurrPass);
                }}
              />
            }
            secureTextEntry={visibleCurrPass}
          />
          <FormField
            autoCorrect={false}
            autoCapitalize="none"
            name="newPassword"
            label="Новый пароль"
            rightIcon={
              <Icon
                name={visibleNewPass ? "eye-off-sharp" : "eye-sharp"}
                type="ionicon"
                onPress={() => {
                  setVisibleNewPass(!visibleNewPass);
                }}
              />
            }
            secureTextEntry={visibleNewPass}
          />
        </RegisterScreen>
      </Form>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    alignSelf: "flex-start",
    marginBottom: 50,
    marginTop: 10,
  },
  signature: {
    color: defaultStyle.colors.medium,
    marginTop: -20,
  },
});
