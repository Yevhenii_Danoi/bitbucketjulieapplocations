import React, { useState } from "react";
import { StyleSheet, Text } from "react-native";
import * as Yup from "yup";
import firebase from "firebase";

import Screen from "../../components/Screen";
import { Form, FormField } from "../../components/forms";
import RegisterScreen from "../../components/RegisterScreen";

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .required("Пожалуйста, заполните это поле")
    .label("Наименование"),
  category: Yup.string().label("Категория"),
  price: Yup.number()
    .required("Пожалуйста, введите стоимость")
    .label("Стоимость"),
});

export default function AddFinanceRecord({ route, navigation }) {
  const { operation } = route.params;

  const [date, setDate] = useState(new Date());

  const onAddRecord = ({ name, category, price }) => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("finance")
      .add({
        name: name,
        category: category,
        price:
          operation === "income"
            ? Math.abs(parseFloat(price).toFixed(2))
            : -Math.abs(parseFloat(price).toFixed(2)),
        date: new Date(date.getTime() - date.getTimezoneOffset() * 60000)
          .toISOString()
          .split("T")[0],
      })
      .then(navigation.goBack());
  };

  return (
    <Screen>
      <Form
        initialValues={{ name: "", category: "", price: "" }}
        validationSchema={validationSchema}
        onSubmit={(values) => onAddRecord(values)}
      >
        <RegisterScreen buttonTitle="Готово">
          <FormField label="Наменование" name="name" />
          <FormField label="Категория (необязательно)" name="category" />
          <FormField
            autoCapitalize="none"
            name="price"
            label="Стоимость"
            keyboardType="numeric"
          />
        </RegisterScreen>
      </Form>
    </Screen>
  );
}

const styles = StyleSheet.create({});
