import React from "react";
import FinanceCard from "../../components/finance/FinancialStatisticsCard";

export default function FinanceScreen({ navigation }) {
  return <FinanceCard navigation={navigation} />;
}
