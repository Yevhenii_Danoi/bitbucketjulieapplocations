import React, { useEffect, useState } from "react";
import { Linking, Platform, StyleSheet, Text, View, Share } from "react-native";
import * as Clipboard from "expo-clipboard";
import { Avatar, Button, Icon, ThemeProvider } from "react-native-elements";
import firebase from "firebase";
import { StackActions } from "@react-navigation/native";
import * as SMS from "expo-sms";

import { monthsNamesInDeclension } from "../../config/calendar";
import { getCapitalLetters } from "../../config/utils";
import routes from "../../navigation/routes";
import defaultStyle from "../../config/styles";
import OverlayAI from "../../components/OverlayAI";
import {
  TouchableOpacity,
  GestureHandlerRootView,
} from "react-native-gesture-handler";

export default function AboutClientRecordScreen({ route, navigation }) {
  const [isLoaded, setIsLoaded] = useState(false);

  const copyNumber = () => {
    let phone = phoneNumber
      .replace(" ", "")
      .replace("(", "")
      .replace(")", "")
      .replace("-", "");
    Clipboard.setString(phone);
  };

  const [smsReminder, setSmsReminder] = useState("");
  useEffect(() => {
    async function fetchData() {
      await firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .get()
        .then((doc) => {
          setSmsReminder(doc.data().smsReminder);
        });
    }
    fetchData();
  }, []);

  const {
    clientID,
    name = "Неизвестный",
    avatar = "null",
    procedure,
    startTime,
    endTime,
    date,
    phoneNumber = "Неизвестен",
    color,
    fromOnline,
  } = route.params.client;

  //https://stackoverflow.com/questions/847185/convert-a-unix-timestamp-to-time-in-javascript
  const toHHMM = (seconds) => {
    let unix_timestamp = seconds;
    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ":" + minutes.slice(-2);

    return formattedTime;
  };

  const onSendRemind = async () => {
    console.log(startTime);
    //First method - many choices
    const result = await Share.share({
      message: smsReminder
        .replace("[Имя клиента]", name)
        .replace(
          "[Дата]",
          `${date.split("-")[2]} ${
            monthsNamesInDeclension[date.split("-")[1] - 1]
          }`
        )
        .replace("[Время]", toHHMM(startTime.seconds)),
    });
    if (result.action === Share.sharedAction) {
      console.log("Share was succesfull");
    } else console.log("share was dismissed");

    //Second method via Viber
    // ("whatsapp://send?phone=79xxxxxxxxx");
    Clipboard.setString(
      phoneNumber
        .replace("(", "")
        .replace(")", "")
        .replace(" ", "")
        .replace("-", "")
    );
    // Linking.openURL(
    //   "http://play.google.com/store/apps/details?id=com.google.android.apps.maps"
    // );
    // await Linking.openURL(
    //   `viber://chat?number=38${phoneNumber
    //     .replace("(", "")
    //     .replace(")", "")
    //     .replace(" ", "")
    //     .replace("-", "")}`
    // )
    //   .then(() => console.log("Succesfull"))
    //   .catch((err) => Alert.alert("Ошибка", err));

    //Third method via SMS
    // const isAvailable = await SMS.isAvailableAsync();
    // if (isAvailable) {
    //   await SMS.sendSMSAsync(
    //     ["380975589660"],
    //     smsReminder
    //       .replace("[Имя клиента]", name)
    //       .replace(
    //         "[Дата]",
    //         `${date.split("-")[2]} ${
    //           monthsNamesInDeclension[date.split("-")[1] - 1]
    //         }`
    //       )
    //     // {
    //     //   attachments: {
    //     //     uri: "path/myfile.png",
    //     //     mimeType: "image/png",
    //     //     filename: "myfile.png",
    //     //   },
    //     // }
    //   );
    // } else {
    //   Alert.alert(
    //     "Невозможно отрпавить ссобщение",
    //     "С вашего устройства невозможно отправить сообщение"
    //   );
    // }
  };

  const makeCall = () => {
    let phone = "";
    if (Platform.OS === "android") {
      phone = `tel:${phoneNumber}`;
    } else {
      phone = `telprompt:${phoneNumber}`;
    }

    Linking.openURL(phone);
  };

  return (
    <>
      <OverlayAI visible={isLoaded} />
      <ThemeProvider theme={defaultStyle.theme}>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <View style={styles.container}>
            <View style={styles.nameContainer}>
              <Avatar
                title={getCapitalLetters(name)}
                titleStyle={{ fontSize: 26 }}
                rounded
                size={100}
                // source={{ uri: avatar }}
                containerStyle={[styles.avatar, { backgroundColor: color }]}
              />
              <Text style={[styles.text, styles.nameText]}>{name}</Text>
              <Text style={styles.text}>
                {fromOnline && <Text>Онлайн-запись</Text>}
              </Text>
            </View>

            <View style={styles.contactContainer}>
              <View style={styles.iconTextContainer}>
                <Icon
                  type="material-community"
                  name="cellphone-android"
                  size={30}
                  style={styles.icon}
                />
                <Text style={styles.text}>
                  Номер телефона: {phoneNumber || "без номера"}
                </Text>
                <Icon
                  type="material-community"
                  name="content-copy"
                  onPress={copyNumber}
                />
              </View>
              <View style={styles.iconTextContainer}>
                <Icon
                  type="fontisto"
                  name="date"
                  size={30}
                  style={styles.icon}
                />
                <Text style={styles.text}>
                  Дата записи: {date.split("-")[2]}{" "}
                  {monthsNamesInDeclension[date.split("-")[1] - 1]}{" "}
                  {date.split("-")[0]}
                </Text>
              </View>
              <View style={styles.iconTextContainer}>
                <Icon
                  type="material-community"
                  name="clock-time-eight-outline"
                  size={30}
                  style={styles.icon}
                />
                <Text style={styles.text}>
                  Время посещения: {toHHMM(startTime.seconds)} –{" "}
                  {toHHMM(endTime.seconds)}
                </Text>
              </View>
              <View style={styles.iconTextContainer}>
                <Icon
                  type="octicon"
                  name="note"
                  size={30}
                  style={styles.icon}
                />
                <Text style={styles.text}>Услуга: {procedure}</Text>
              </View>
            </View>

            <View style={styles.functionalContainer}>
              <TouchableOpacity
                style={styles.iconTextContainer}
                onPress={() => {
                  onSendRemind();
                }}
              >
                <Text style={[styles.text, { marginRight: 10 }]}>
                  Отправить напоминание
                </Text>
                <Icon name="viber" type="fontisto" style={styles.icon} />
                <Icon
                  name="telegram"
                  type="material-community"
                  style={styles.icon}
                />
                <Icon name="whatsapp" type="font-awesome" style={styles.icon} />
              </TouchableOpacity>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 3 }}>
                  <Button
                    title="История записей"
                    icon={
                      <Icon
                        type="material-community"
                        name="history"
                        size={30}
                        color={defaultStyle.colors.white}
                      />
                    }
                    onPress={() =>
                      navigation.navigate(routes.CLIENT_HISTORY, { clientID })
                    }
                  />
                </View>

                <View style={{ flex: 1 }}>
                  <Button
                    icon={
                      <Icon
                        type="font-awesome"
                        name="phone"
                        size={30}
                        color={defaultStyle.colors.white}
                      />
                    }
                    buttonStyle={styles.callButton}
                    disabled={name === "Неизвестный" ? true : false}
                    onPress={makeCall}
                  />
                </View>
              </View>
            </View>
          </View>
        </GestureHandlerRootView>
      </ThemeProvider>
    </>
  );
}
const styles = StyleSheet.create({
  about: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  avatar: {
    alignSelf: "center",
    marginBottom: 15,
  },
  callButton: {
    backgroundColor: defaultStyle.colors.green,
    marginLeft: 10,
  },
  contactContainer: {
    marginVertical: 15,
  },
  container: {
    flex: 1,
    padding: 15,
  },
  iconTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 15,
  },
  functionalContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  icon: {
    marginRight: 10,
  },
  nameContainer: {
    alignItems: "center",
  },
  nameText: {
    fontWeight: "700",
    fontSize: 22,
  },
  text: {
    fontSize: 18,
    letterSpacing: 0.4,
    textAlign: "center",
  },
});
