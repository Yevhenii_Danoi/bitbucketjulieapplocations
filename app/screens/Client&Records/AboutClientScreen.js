import React, { useState } from "react";
import {
  TouchableOpacity,
  Pressable,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Alert,
} from "react-native";
import { Avatar, Icon, Overlay } from "react-native-elements";
import * as Clipboard from "expo-clipboard";
import firebase from "firebase";

import { getCapitalLetters } from "../../config/utils";
import { makeCall } from "../../modules/utils/makeCall";
import { monthsNamesInDeclension } from "../../config/calendar";
import routes from "../../navigation/routes";
import ActionWindow from "../../components/ActionWindow";
import DeleteOverlay from "../../components/DeleteOverlay";

export default function AboutClientScreen({ route, navigation }) {
  const { id, name, avatar, phoneNumber, lastVisit, color } =
    route.params.client;

  const [visible, setVisible] = useState(false);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Icon
          type="material-community"
          name="dots-vertical"
          onPress={() => toggleOverlay()}
        />
      ),
    });
  }, [navigation]);

  const copyToClipboard = () => {
    Clipboard.setString(id);
  };
  const [lastVisitString, setLastVisitString] = useState(
    lastVisit === "не был"
      ? "не был"
      : `${lastVisit.split("-")[2]} ${
          monthsNamesInDeclension[lastVisit.split("-")[1] - 1]
        } ${lastVisit.split("-")[0]}`
  );
  const [deleting, setDeleting] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const onDeleteRecord = () => {
    setDeleting(true);
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("clients")
      .doc(id)
      .delete()
      .then(() => {
        navigation.goBack();
      })
      .catch((error) =>
        Alert.alert(
          "По некоторым причинам мы не смогли удалить клиента. Попробуйте снова",
          error
        )
      );
  };

  return (
    <>
      {deleting && <DeleteOverlay />}
      <View style={styles.container}>
        <View style={styles.nameContainer}>
          <Avatar
            title={getCapitalLetters(name)}
            titleStyle={{ fontSize: 26 }}
            rounded
            size={100}
            // source={{ uri: avatar }}
            containerStyle={[styles.avatar, { backgroundColor: color }]}
          />
          <Text style={[styles.text, styles.nameText]}>{name}</Text>
          <Text style={styles.text}>
            Последний раз был(а): {lastVisitString}
          </Text>
        </View>

        <View style={styles.contactContainer}>
          <View style={styles.iconTextContainer}>
            <Icon
              type="material-community"
              name="cellphone-android"
              size={30}
              style={styles.icon}
            />
            <Text style={styles.text}>
              Номер телефона: {phoneNumber || "без номера"}
            </Text>
          </View>
        </View>

        <View style={styles.functionalContainer}>
          <TouchableOpacity onPress={() => makeCall(phoneNumber)}>
            <View style={styles.iconTextContainer}>
              <Icon
                type="font-awesome"
                name="phone"
                size={30}
                style={styles.icon}
              />
              <Text style={styles.text}>Позвонить</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={copyToClipboard}>
            <View style={styles.iconTextContainer}>
              <Icon
                type="material-community"
                name="content-copy"
                style={styles.icon}
              />
              <Text style={styles.text}>Копировать код клиента</Text>
            </View>
          </TouchableOpacity>
          <Pressable
            onPress={() =>
              navigation.navigate(routes.CLIENT_HISTORY, { clientID: id })
            }
          >
            <View style={styles.iconTextContainer}>
              <Icon
                type="material-community"
                name="history"
                size={30}
                style={styles.icon}
              />
              <Text style={styles.text}>История записей</Text>
            </View>
          </Pressable>
        </View>
      </View>
      <Overlay
        isVisible={visible}
        onBackdropPress={toggleOverlay}
        overlayStyle={styles.overlay}
      >
        <ActionWindow
          navigation={navigation}
          onEdit={() => {
            toggleOverlay();
            navigation.navigate(routes.EDIT_CLIENT, {
              client: route.params.client,
            });
          }}
          onDelete={() => {
            toggleOverlay();
            Alert.alert("Удалить", `Вы точно хотите удалить ${name}?`, [
              {
                text: "Нет",
                style: "cancel",
              },
              { text: "Да", onPress: () => onDeleteRecord() },
            ]);
          }}
        />
      </Overlay>
    </>
  );
}

const styles = StyleSheet.create({
  overlay: {
    position: "absolute",
    bottom: 0,
    width: Dimensions.get("window").width,
    padding: 0,
  },
  about: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  avatar: {
    alignSelf: "center",
    marginBottom: 15,
  },
  contactContainer: {
    marginVertical: 45,
  },
  container: {
    flex: 1,
    padding: 15,
  },
  iconTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 15,
  },
  functionalContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  icon: {
    marginRight: 10,
  },
  nameContainer: {
    alignItems: "center",
  },
  nameText: {
    fontWeight: "700",
    fontSize: 22,
  },
  text: {
    fontSize: 18,
    letterSpacing: 0.4,
    textAlign: "center",
  },
});
