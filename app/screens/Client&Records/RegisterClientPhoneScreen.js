import React, { useState } from "react";
import { Text } from "react-native";
import * as Yup from "yup";
import firebase from "firebase";
import randomColor from "randomcolor";

import defaultStyle from "../../config/styles";
import RegisterScreen from "../../components/RegisterScreen";
import { ErrorMessage, Form, FormField } from "../../components/forms";
import routes from "../../navigation/routes";
import Screen from "../../components/Screen";

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const validationSchema = Yup.object().shape({
  phone: Yup.string()
    .label("Имя")
    .matches(phoneRegExp, "Номер не действительный"),
});

export default function RegisterClientNameScreen({ route, navigation }) {
  const [error, setError] = useState("");
  const { name, surname } = route.params;

  const docRef = firebase
    .firestore()
    .collection("users")
    .doc(firebase.auth().currentUser.uid)
    .collection("clients");

  const handleAddNewUser = (phone) => {
    const color = randomColor();
    docRef
      .get()
      .then(() => {
        docRef.add({
          name: `${name} ${surname}`,
          phoneNumber: phone,
          avatar: "null",
          lastVisit: "не был",
          color: color,
        });
      })
      .then(() => {
        navigation.navigate(routes.CLIENT_LIST);
      })
      .catch((error) => {
        setError(error);
      });
  };

  return (
    <Screen>
      <Form
        initialValues={{ phone: "" }}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          handleAddNewUser(values.phone);
        }}
      >
        <RegisterScreen buttonTitle="Создать клиента">
          <ErrorMessage error={error} visible={true} />
          <Text style={defaultStyle.mainText}>Как связаться с клиентом?</Text>
          <FormField
            keyboardType="numeric"
            placeholder="Номер телефона"
            label="Номер (необязательно)"
            name="phone"
          />
        </RegisterScreen>
      </Form>
    </Screen>
  );
}
