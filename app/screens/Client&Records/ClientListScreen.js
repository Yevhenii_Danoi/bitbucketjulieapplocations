import React from "react";
import { Pressable, StyleSheet, View } from "react-native";

import routes from "../../navigation/routes";
import ClientList from "../../components/ClientList";
import AddButton from "../../components/AddButton";

export default function ClientListScreen({ navigation }) {
  const getAboutClient = (item) => {
    navigation.navigate(routes.ABOUT_CLIENT, { client: item });
  };
  return (
    <View style={styles.container}>
      <View style={styles.clientsListContainer}>
        <ClientList onPress={getAboutClient} />
      </View>

      <AddButton onPress={() => navigation.navigate(routes.ADD_CLIENT)} />
    </View>
  );
}

const styles = StyleSheet.create({
  clientsListContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  text: {
    fontSize: 20,
    padding: 15,
  },
});
