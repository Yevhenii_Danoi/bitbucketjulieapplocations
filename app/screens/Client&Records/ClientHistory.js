import React, { useEffect, useState } from "react";
import { SectionList, StyleSheet, Text, View } from "react-native";
import firebase from "firebase";

import ProcedureCard from "../../components/ProcedureCard";
import BackgroundPicture from "../../components/BackgroundPicture";
import defaultStyles from "../../config/styles";
import ProjectActivityIndicator from "../../components/ProjectActivityIndicator";
export default function ClientHistory({ route }) {
  const { clientID } = route.params;

  const [clientRecords, setClientRecords] = useState([]);

  const [listData, setListData] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    setIsLoaded(false);

    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("records")
      .where("clientID", "==", clientID)
      .get()
      .then((snapshot) => {
        const data = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        data.sort((a, b) => (a.date < b.date ? 1 : -1));
        setClientRecords(data);
      });
  }, []);

  //https://stackoverflow.com/questions/54055950/sort-and-group-react-native-sectionlist-by-key
  useEffect(() => {
    const groupNames = Array.from(
      new Set(clientRecords.map((rec) => rec.date.split("-")[0]))
    );

    let groups = {};

    groupNames.forEach((year) => {
      groups[year] = [];
    });

    clientRecords.forEach((rec) => {
      const year = rec.date.split("-")[0];
      groups[year].push(rec);
    });

    let recordsArray = [];
    let data = [];
    for (let key in groups) {
      let obj = { title: key };
      for (let record of groups[key]) {
        recordsArray.push(record);
      }
      obj["length"] = recordsArray.length;
      obj["data"] = recordsArray;
      data.push(obj);
      recordsArray = [];
    }
    data.sort((a, b) => (parseInt(a.title) > parseInt(b.title) ? -1 : 1));
    setListData(data);
  }, [clientRecords]);

  useEffect(() => {
    setTimeout(() => {
      setIsLoaded(true);
    }, 1000);
  }, [listData]);

  if (isLoaded && listData.length > 0) {
    return (
      <View style={styles.container}>
        <SectionList
          renderItem={({ item }) => <ProcedureCard record={item} />}
          renderSectionHeader={({ section: { title, length } }) => (
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{title}</Text>
              <Text style={styles.title}>Записей: {length}</Text>
            </View>
          )}
          sections={listData}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    );
  } else if (isLoaded && listData.length === 0) {
    return (
      <BackgroundPicture source={require("../../assets/images/history.png")}>
        <Text style={defaultStyles.regularText}>
          Здесь отобразится история посещений клиента
        </Text>
      </BackgroundPicture>
    );
  }

  return <ProjectActivityIndicator />;
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  titleContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 15,
    marginTop: 15,
  },
  title: {
    fontWeight: "bold",
    fontSize: 16,
  },
});
