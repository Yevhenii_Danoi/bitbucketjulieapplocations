import React from "react";
import { Text } from "react-native";
import * as Yup from "yup";

import defaultStyle from "../../config/styles";
import RegisterScreen from "../../components/RegisterScreen";
import { Form, FormField } from "../../components/forms";
import routes from "../../navigation/routes";
import Screen from "../../components/Screen";

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Пожалуйста, введите имя клиента").label("Имя"),
  surname: Yup.string().min(1).label("Фамилия"),
});

export default function RegisterClientNameScreen({ navigation }) {
  return (
    <Screen>
      <Form
        initialValues={{ name: "", surname: "" }}
        validationSchema={validationSchema}
        onSubmit={(values) =>
          navigation.navigate(routes.REGISTER_CLIENT_PHONE, {
            name: values.name,
            surname: values.surname,
          })
        }
      >
        <RegisterScreen buttonTitle="Далее">
          <Text style={defaultStyle.mainText}>Как зовут вашего клиента?</Text>
          <FormField placeholder="Имя" label="Имя" name="name" />
          <FormField
            placeholder="Фамилия"
            label="Фамилия (необязательно)"
            name="surname"
          />
        </RegisterScreen>
      </Form>
    </Screen>
  );
}
