import React, { useEffect, useState } from "react";
import { Alert, StyleSheet, Text, View } from "react-native";
import { Avatar } from "react-native-elements";
import {
  TouchableOpacity,
  GestureHandlerRootView,
} from "react-native-gesture-handler";
import firebase from "firebase";

import { getCapitalLetters } from "../../config/utils";
import TextInput from "../../components/TextInput";
import OverlayAI from "../../components/OverlayAI";
import routes from "../../navigation/routes";

export default function EditClient({ route, navigation }) {
  const { name, phoneNumber, avatar, id, color } = route.params.client;

  const [newName, onNameChange] = useState(name);
  const [newPhone, onPhoneChange] = useState(phoneNumber);
  const [saving, setSaving] = useState(false);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <GestureHandlerRootView>
          <TouchableOpacity onPress={() => setSaving(true)}>
            <Text style={styles.text}>Готово</Text>
          </TouchableOpacity>
        </GestureHandlerRootView>
      ),
    });
  }, [navigation]);

  useEffect(() => {
    if (saving) {
      firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .collection("clients")
        .doc(id)
        .update({
          name: newName,
          phoneNumber: newPhone,
        })
        .then(() => {
          navigation.navigate(routes.ABOUT_CLIENT, {
            client: {
              id,
              name: newName,
              phoneNumber: newPhone,
              avatar,
              lastVisit: route.params.client.lastVisit,
              color: color,
            },
          });
        })
        .catch(() => Alert.alert("Извините, мы не смогли изменить имя"));
    }
  }, [saving]);

  return (
    <>
      {saving && <OverlayAI visible={true} />}
      <View style={styles.container}>
        <View style={styles.nameContainer}>
          <Avatar
            title={getCapitalLetters(name)}
            titleStyle={{ fontSize: 26 }}
            rounded
            size={100}
            source={{ uri: avatar }}
            containerStyle={[styles.avatar, { backgroundColor: color }]}
          />
        </View>

        <TextInput
          label="Имя"
          value={newName}
          onChangeText={onNameChange}
          placeholder="Имя"
        />
        <TextInput
          label="Номер"
          defaultValue={phoneNumber}
          onChangeText={onPhoneChange}
          keyboardType="numeric"
          placeholder="Номер телефона"
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
    textTransform: "uppercase",
    letterSpacing: 0.8,
  },
  about: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  avatar: {
    alignSelf: "center",
    marginBottom: 15,
  },
  contactContainer: {
    marginVertical: 45,
  },
  container: {
    flex: 1,
    padding: 15,
  },
  iconTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 15,
  },
  functionalContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
  icon: {
    marginRight: 10,
  },
  nameContainer: {
    alignItems: "center",
  },
  nameText: {
    fontWeight: "700",
    fontSize: 22,
  },
  text: {
    fontSize: 18,
    letterSpacing: 0.4,
    textAlign: "center",
  },
});
