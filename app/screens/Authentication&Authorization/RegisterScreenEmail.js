import React from "react";
import { StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import * as Yup from "yup";

import { ErrorMessage } from "../../components/forms";
import RegisterScreen from "../../components/RegisterScreen";
import defaultStyle from "../../config/styles";
import { Form, FormField } from "../../components/forms";
import routes from "../../navigation/routes";
import useAuth from "../../auth/useAuth";
import Screen from "../../components/Screen";

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Пожалуйста, введите свой эл.адрес")
    .email("Электронная почта должна быть действительной")
    .label("Email"),
});

export default function RegisterScreenEmail({ navigation }) {
  const { setError, error } = useAuth();
  return (
    <Screen>
      <Form
        initialValues={{ email: "" }}
        onSubmit={(values) => {
          navigation.navigate(routes.CHOOSE_PASSWORD, {
            email: values.email,
          });
          setError("");
        }}
        validationSchema={validationSchema}
      >
        <RegisterScreen authScreen buttonTitle="Далее">
          <Text style={defaultStyle.mainText}>Добро пожаловать</Text>
          <Text style={defaultStyle.mainText}>Какой ваш email?</Text>
          <ErrorMessage error={error} visible={true} />
          <FormField
            autoCapitalize="none"
            name="email"
            placeholder="Email"
            label="Email"
          />
        </RegisterScreen>
      </Form>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
