import React from "react";
import { Text } from "react-native";
import * as Yup from "yup";

import defaultStyle from "../../config/styles";
import RegisterScreen from "../../components/RegisterScreen";
import { ErrorMessage, Form, FormField } from "../../components/forms";
import useAuth from "../../auth/useAuth";
import Screen from "../../components/Screen";
import OverlayAI from "../../components/OverlayAI";

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Пожалуйста, введите свое имя").label("Имя"),
  surname: Yup.string().min(1).label("Фамилия"),
});

export default function WhatsYourNameScreen({ route }) {
  const { email, password } = route.params;
  const { signUp, error, isLoading } = useAuth();

  const handleSubmit = (values) => {
    signUp(email, password, values);
  };

  return (
    <Screen>
      <OverlayAI visible={isLoading} />
      <Form
        initialValues={{ name: "", surname: "" }}
        validationSchema={validationSchema}
        onSubmit={(values) => handleSubmit(values)}
      >
        <RegisterScreen authScreen buttonTitle="Создать аккаунт">
          <Text style={defaultStyle.mainText}>Как вас зовут?</Text>
          <ErrorMessage error={error} visible={true} />
          <FormField placeholder="Имя" label="Имя" name="name" />
          <FormField
            placeholder="Фамилия"
            label="Фамилия (необязательно)"
            name="surname"
          />
        </RegisterScreen>
      </Form>
    </Screen>
  );
}
