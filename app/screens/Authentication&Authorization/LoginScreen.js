import React, { useState } from "react";
import { Alert, Pressable, StyleSheet } from "react-native";
import { Icon, Text } from "react-native-elements";
import { Form, FormField } from "../../components/forms";
import DialogInput from "react-native-dialog-input";
import * as Yup from "yup";

import RegisterScreen from "../../components/RegisterScreen";
import defaultStyle from "../../config/styles";
import useAuth from "../../auth/useAuth";
import { ErrorMessage } from "../../components/forms";
import Screen from "../../components/Screen";
import OverlayAI from "../../components/OverlayAI";

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Пожалуйста, введите свой эл.адрес")
    .email("Электронная почта должна быть действительной")
    .label("Email"),
  password: Yup.string().required("Пожалуйста, введите пароль").label("Пароль"),
});
export default function LoginScreen() {
  const { login, error, isLoading, resetPassword } = useAuth();
  const [visible, setVisible] = useState(true);
  const [isDialogVisible, setIsDialogVisible] = useState(false);

  const onLogin = (values) => {
    login(values.email, values.password);
  };
  return (
    <Screen>
      <OverlayAI visible={isLoading} />
      <Form
        initialValues={{ email: "", password: "" }}
        onSubmit={(values) => onLogin(values)}
        validationSchema={validationSchema}
      >
        <RegisterScreen authScreen buttonTitle="Войти">
          <Text style={defaultStyle.mainText}>С возвращением</Text>
          <ErrorMessage error={error} visible={true} />
          <FormField
            autoCapitalize="none"
            name="email"
            placeholder="Email"
            label="Email"
          />
          <FormField
            autoCorrect={false}
            autoCapitalize="none"
            name="password"
            label="Пароль"
            placeholder="Пароль"
            rightIcon={
              <Icon
                name={visible ? "eye-off-sharp" : "eye-sharp"}
                type="ionicon"
                onPress={() => {
                  setVisible(!visible);
                }}
              />
            }
            secureTextEntry={visible}
            textContentType="password"
          />
          <Pressable
            style={{ marginTop: -20 }}
            onPress={() => setIsDialogVisible(true)}
          >
            <Text style={{ fontSize: 15 }}>Забыли пароль?</Text>
          </Pressable>

          <DialogInput
            isDialogVisible={isDialogVisible}
            title={"Введите email"}
            message={"Введите email для сброса пароля"}
            hintInput={"Email"}
            submitInput={(email) => {
              resetPassword(email);
              setIsDialogVisible(false);
            }}
            closeDialog={() => setIsDialogVisible(false)}
          ></DialogInput>
        </RegisterScreen>
      </Form>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
