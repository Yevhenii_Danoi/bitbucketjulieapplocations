import React, { useState } from "react";
import { Text, Icon } from "react-native-elements";
import * as Yup from "yup";

import { ErrorMessage } from "../../components/forms";
import defaultStyle from "../../config/styles";
import RegisterScreen from "../../components/RegisterScreen";
import { Form, FormField } from "../../components/forms";
import routes from "../../navigation/routes";
import Screen from "../../components/Screen";
import useAuth from "../../auth/useAuth";

const passwordRestrictionText =
  "8-30 символов, Одна заглавная, Одна прописная, Одна цифра";

const validationSchema = Yup.object().shape({
  newPassword: Yup.string()
    .required("Пожалуйста, выберите для себя пароль")
    .label("Пароль")
    .matches(
      /\w*[a-z]\w*/,
      passwordRestrictionText //маленькая буква
    )
    .matches(
      /\w*[A-Z]\w*/,
      passwordRestrictionText //большая буква
    )
    .matches(/\d/, passwordRestrictionText) //цифра
    .min(8, passwordRestrictionText)
    .max(30, passwordRestrictionText),
});

export default function RegisterChoosePasswordScreen({ route, navigation }) {
  const { email } = route.params;
  const [visible, setVisible] = useState(true);
  const { error } = useAuth();

  return (
    <Screen>
      <Form
        initialValues={{ newPassword: "" }}
        onSubmit={(values) =>
          navigation.navigate(routes.CHOOSE_NAME, {
            email,
            password: values.newPassword,
          })
        }
        validationSchema={validationSchema}
      >
        <RegisterScreen authScreen buttonTitle="Далее">
          <Text style={defaultStyle.mainText}>Выберите пароль</Text>
          <ErrorMessage error={error} visible={true} />
          <FormField
            autoCorrect={false}
            autoCapitalize="none"
            name="newPassword"
            label="Пароль"
            placeholder="Пароль"
            rightIcon={
              <Icon
                name={visible ? "eye-off-sharp" : "eye-sharp"}
                type="ionicon"
                onPress={() => {
                  setVisible(!visible);
                }}
              />
            }
            secureTextEntry={visible}
            textContentType="password"
          />
        </RegisterScreen>
      </Form>
    </Screen>
  );
}
