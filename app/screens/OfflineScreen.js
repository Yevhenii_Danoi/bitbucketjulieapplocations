import React from "react";
import { StyleSheet, Text } from "react-native";
import { Button } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";

import defaultStyle from "../config/styles";

export default function OfflineScreen() {
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.text}>Нет подключения к Интернету</Text>
      <Text>Ожидание подключения...</Text>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    fontSize: 16,
    marginBottom: 15,
    color: defaultStyle.colors.black,
  },
});
