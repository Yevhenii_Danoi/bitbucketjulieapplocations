import React, { useState, useEffect } from "react";
import { Animated, Easing, StyleSheet, View } from "react-native";
import { Text } from "react-native-elements";
import { Calendar, LocaleConfig } from "react-native-calendars";
import { SafeAreaView } from "react-native-safe-area-context";
import LottieView from "lottie-react-native";
import firebase from "firebase";
import AsyncStorage from "@react-native-async-storage/async-storage";

import defaultStyle from "../config/styles";
import ClientCardList from "../components/ClientCardList";
import { monthsNamesInDeclension } from "../config/calendar";
import routes from "../navigation/routes";
import AddButton from "../components/AddButton";
import FirstLaunchModal from "../components/FirstLaunchModal";

export default function PlannerScreen({ navigation }) {
  LocaleConfig.defaultLocale = "ru";

  const currentDate = new Date();

  const [date, setDate] = useState(
    new Date(currentDate.getTime() - currentDate.getTimezoneOffset() * 60000)
      .toISOString()
      .split("T")[0]
  );
  const [modalVisible, setModalVisible] = useState(false);

  const [dateSelected, setDateSelected] = useState({
    [date]: { selected: true, selectedColor: "#466A8F" },
  });
  const [progress, setProgress] = useState(new Animated.Value(0));
  const [animateStyles, setAnimateStyles] = useState({});
  const [invitationLink, setInvitationLink] = useState("");
  useEffect(() => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .get()
      .then((doc) => {
        setInvitationLink(doc.data().recordingUrl);
      });
  }, []);

  useEffect(() => {
    AsyncStorage.getItem("modalOnFirstLaunchShown").then((value) => {
      if (!JSON.parse(value)) {
        setModalVisible(true);
      }
    });
  }, []);

  const getAboutClient = (item) => {
    navigation.navigate(routes.CLIENT_RECORD_PARAMETER, { client: item });
  };

  const showAnimation = () => {
    setAnimateStyles({ zIndex: 1 });
    Animated.timing(progress, {
      toValue: 1,
      duration: 2500,
      useNativeDriver: true,
      easing: Easing.sin,
    }).start(({ finished }) => {
      if (finished) {
        setAnimateStyles({ zIndex: -1 });
        setProgress(new Animated.Value(0));
      }
    });
  };

  return (
    <>
      <FirstLaunchModal
        modalVisible={modalVisible}
        invitationLink={invitationLink}
        onClose={() => {
          AsyncStorage.setItem(
            "modalOnFirstLaunchShown",
            JSON.stringify(true)
          ).then(() => {
            setModalVisible(!modalVisible);
          });
        }}
      />
      <LottieView
        source={require("../assets/animations/confetti.json")}
        progress={progress}
        style={animateStyles}
      />
      <SafeAreaView style={styles.container}>
        <Text style={styles.text}>
          Планировщик {monthsNamesInDeclension[currentDate.getMonth()]}
        </Text>
        <Calendar
          style={{ marginBottom: 25 }}
          onDayPress={(day) => {
            setDate(day);
            setDateSelected({
              [day.dateString]: { selected: true, selectedColor: "#466A8F" },
            });
          }}
          firstDay={1}
          enableSwipeMonths={true}
          markedDates={dateSelected}
        />
        <View style={{ flex: 1 }}>
          <ClientCardList
            date={date.dateString ? date.dateString : date}
            navigation={navigation}
            animation={showAnimation}
          />
        </View>
        <AddButton
          onPress={() =>
            navigation.navigate(routes.CLIENT_LIST, {
              onPress: getAboutClient,
            })
          }
        />
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: defaultStyle.colors.light,
  },
  text: {
    color: defaultStyle.colors.black,
    fontSize: 20,
    fontWeight: "700",
    padding: 15,
  },
});
