import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";

import WelcomeScreen from "../screens/WelcomeScreen";
import RegisterScreenEmail from "../screens/Authentication&Authorization/RegisterScreenEmail";
import RegisterChoosePasswordScreen from "../screens/Authentication&Authorization/RegisterChoosePasswordScreen";
import LoginScreen from "../screens/Authentication&Authorization/LoginScreen";
import WhatsYourNameScreen from "../screens/Authentication&Authorization/WhatsYourNameScreen";
import routes from "./routes";

const Stack = createStackNavigator();

export default function AuthNavigator() {
  return (
    <Stack.Navigator initialRouteName={routes.WELCOME}>
      <Stack.Screen
        name={routes.WELCOME}
        component={WelcomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={routes.LOGIN}
        component={LoginScreen}
        options={{ title: "Войти" }}
      />
      <Stack.Screen
        name={routes.CHOOSE_EMAIL}
        component={RegisterScreenEmail}
        options={{ title: "Регистрация" }}
      />
      <Stack.Screen
        name={routes.CHOOSE_PASSWORD}
        component={RegisterChoosePasswordScreen}
        options={{ title: "Регистрация" }}
      />
      <Stack.Screen
        name={routes.CHOOSE_NAME}
        component={WhatsYourNameScreen}
        options={{ title: "Регистрация" }}
      />
    </Stack.Navigator>
  );
}
