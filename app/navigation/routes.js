export default Object.freeze({
  ABOUT_CLIENT: "AboutClient",
  ABOUT_CLIENT_RECORD: "AboutClientRecord",
  ACCOUNT_DETAILS: "AccountDetails",
  ADD_CLIENT: "AddClient",
  ADD_AVATAR: "AddAvatar",
  APP_NAVIGATOR: "AppNavigator",
  CHANGE_PASSWORD: "ChangePassword",
  CHOOSE_AREA_WORK: "ChoiceAreasWork",
  CHOOSE_EMAIL: "ChoooseEmail",
  CHOOSE_PASSWORD: "ChoosePassword",
  CHOOSE_NAME: "ChooseName",
  CLIENT_CARD: "ClientCard",
  CLIENT_CARD_LIST: "ClientCardList",
  CLIENT_LIST: "ClientList",
  CLIENT_RECORD_PARAMETER: "ClientRecordParameter",
  CLIENTS: "Clients",
  CLIENT_LIST: "ClientList",
  CLIENT_HISTORY: "ClientHistory",
  EDIT_CLIENT: "EditClient",
  FINANCE_SCREEN: "FinanceScreen",
  FINANCE_STACK: "FinanceStackScreen",
  FINANCE_CREATE_RECORD: "CreateFinanceRecord",
  PLANNER: "Planner",
  PLANNER_STACK: "PlannerScreen",
  PRICE_LIST_SCREEN: "PriceListScreen",
  PROFILE_SETTINGS_STACK: "ProfileSettingsStack",
  PROFILE_SETTINGS_SCREEN: "ProfileSettingsScreen",
  SCHEDULING: "Scheduling",
  SETTINGS_STACK: "SettingsStack",
  SETTINGS_SCREEN: "SettingsScreen",
  REGISTRATION_SETTINGS: "RegistrationSettings",
  REGISTER_CLIENT_NAME: "RegisterClientName",
  REGISTER_CLIENT_PHONE: "RegisterPhoneClient",
  LOGIN: "Login",
  WELCOME: "Welcome",
});
