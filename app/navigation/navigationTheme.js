import { DefaultTheme } from "@react-navigation/native";
import defaultStyle from "../config/styles";

export default {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: defaultStyle.colors.black,
    background: defaultStyle.colors.white,
    notification: defaultStyle.colors.secondary,
  },
};
