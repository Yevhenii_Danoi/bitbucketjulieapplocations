import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import SchedulingScreen from "../screens/InitialAccountSetup/SchedulingScreen";
import routes from "./routes";
import ChoiceAreasWork from "../screens/InitialAccountSetup/ChoiceAreasWork";
import PriceListScreen from "../screens/PriceListScreen";
import AdddingAvatar from "../screens/InitialAccountSetup/AddingAvatar";

const Stack = createStackNavigator();

export default BasicAccountSettings = () => {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName={routes.SCHEDULING}
    >
      <Stack.Screen name={routes.SCHEDULING} component={SchedulingScreen} />
      <Stack.Screen
        name={routes.CHOOSE_AREA_WORK}
        component={ChoiceAreasWork}
      />
      <Stack.Screen
        name={routes.PRICE_LIST_SCREEN}
        component={PriceListScreen}
        options={{ headerShown: true, title: "Редактировать прайс-лист" }}
      />
      <Stack.Screen name={routes.ADD_AVATAR} component={AdddingAvatar} />
    </Stack.Navigator>
  );
};
