import "react-native-gesture-handler";
import React, { useEffect } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Icon } from "react-native-elements";

import AddFinanceRecord from "../screens/Finance/AddFinanceRecord";
import AboutClientScreen from "../screens/Client&Records/AboutClientScreen";
import EditClient from "../screens/Client&Records/EditClient";
import AboutClientRecordScreen from "../screens/Client&Records/AboutClientRecordScreen";
import AccountDetailsScreen from "../screens/Settings/AccountDetailsScreen";
import ChangePasswordScreen from "../screens/Settings/ChangePasswordScreen";
import ChoiceAreasWork from "../screens/InitialAccountSetup/ChoiceAreasWork";
import ClientCard from "../components/ClientCard";
import ClientCardList from "../components/ClientCardList";
import ClientHistory from "../screens/Client&Records/ClientHistory";
import ClientList from "../components/ClientList";
import ClientListScreen from "../screens/Client&Records/ClientListScreen";
import CreateClientRecord from "../components/CreateClientRecord";
import FinanceScreen from "../screens/Finance/FinanceScreen";
import PlannerScreen from "../screens/PlannerScreen";
import ProfileSettingsScreen from "../screens/Settings/ProfileSettingsScreen";
import RegisterClientNameScreen from "../screens/Client&Records/RegisterClientNameScreen";
import RegisterPhoneClientScreen from "../screens/Client&Records/RegisterClientPhoneScreen";
import routes from "./routes";
import SettingsScreen from "../screens//Settings/SettingsScreen";
import SchedulingScreen from "../screens/InitialAccountSetup/SchedulingScreen";
import PriceListScreen from "../screens/PriceListScreen";

import { useIsConnected } from "react-native-offline";
import NotificationScreen from "../components/NotificationScreen";
import * as AppConstants from "../config/constants";

const Tab = createBottomTabNavigator();

const AddNewClientStack = createNativeStackNavigator();
const ClientStack = createNativeStackNavigator();
const FinanceStack = createNativeStackNavigator();
const PlannerStack = createNativeStackNavigator();
const ProfileSettingsStack = createNativeStackNavigator();
const SettingsStack = createNativeStackNavigator();
const NotificationStack = createNativeStackNavigator();

function AddNewClientStackScreen() {
  return (
    <AddNewClientStack.Navigator>
      <AddNewClientStack.Screen
        name={routes.REGISTER_CLIENT_NAME}
        component={RegisterClientNameScreen}
        options={{ headerShown: false }}
      />
      <AddNewClientStack.Screen
        name={routes.REGISTER_CLIENT_PHONE}
        component={RegisterPhoneClientScreen}
        options={{ headerShown: false }}
      />
    </AddNewClientStack.Navigator>
  );
}
function PlannerStackScreen() {
  return (
    <PlannerStack.Navigator>
      <PlannerStack.Screen
        name={routes.PLANNER}
        component={PlannerScreen}
        options={{ headerShown: false }}
      />
      <PlannerStack.Screen
        name={routes.CLIENT_CARD_LIST}
        component={ClientCardList}
      />
      <PlannerStack.Screen name={routes.CLIENT_CARD} component={ClientCard} />
      <PlannerStack.Screen
        name={routes.CLIENT_HISTORY}
        component={ClientHistory}
        options={{ title: "История клиента" }}
      />
      <PlannerStack.Screen
        name={routes.CLIENT_LIST}
        component={ClientList}
        options={{ title: "Выбор клиента" }}
      />
      <PlannerStack.Screen
        name={routes.CLIENT_RECORD_PARAMETER}
        component={CreateClientRecord}
        options={{ title: "Создать запись" }}
      />
      <PlannerStack.Screen
        name={routes.ABOUT_CLIENT_RECORD}
        component={AboutClientRecordScreen}
        options={{ title: "Подробности записи" }}
      />
    </PlannerStack.Navigator>
  );
}

function ClientStackScreen() {
  return (
    <ClientStack.Navigator>
      <ClientStack.Screen
        name={routes.CLIENT_LIST}
        component={ClientListScreen}
        options={{ title: "Мои клиенты" }}
      />
      <ClientStack.Screen
        name={routes.ABOUT_CLIENT}
        component={AboutClientScreen}
        options={{ title: "О клиенте" }}
      />
      <ClientStack.Screen
        name={routes.EDIT_CLIENT}
        component={EditClient}
        options={{ title: "Изменить" }}
      />
      <ClientStack.Screen
        name={routes.CLIENT_HISTORY}
        component={ClientHistory}
        options={{ title: "История клиента" }}
      />
      <ClientStack.Screen
        name={routes.ADD_CLIENT}
        component={AddNewClientStackScreen}
        options={{ title: "Новый клиент" }}
      />
    </ClientStack.Navigator>
  );
}

function SettingsStackScreen() {
  return (
    <SettingsStack.Navigator>
      <SettingsStack.Screen
        name={routes.SETTINGS_SCREEN}
        component={SettingsScreen}
        options={{ title: "Настройки" }}
      />
      <SettingsStack.Screen
        name={routes.PROFILE_SETTINGS_STACK}
        component={ProfileSettingsStackScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name={routes.ACCOUNT_DETAILS}
        component={AccountDetailsScreen}
        options={{ title: "Подробности аккаунта" }}
      />
      <SettingsStack.Screen
        name={routes.CHANGE_PASSWORD}
        component={ChangePasswordScreen}
        options={{
          headerShown: false,
        }}
      />
    </SettingsStack.Navigator>
  );
}

function FinanceStackScreen() {
  return (
    <FinanceStack.Navigator>
      <FinanceStack.Screen
        name={routes.FINANCE_SCREEN}
        component={FinanceScreen}
        options={{ title: "Статистика и финансы" }}
      />
      <FinanceStack.Screen
        name={routes.FINANCE_CREATE_RECORD}
        component={AddFinanceRecord}
        options={{ title: "Редактировать" }}
      />
    </FinanceStack.Navigator>
  );
}

function ProfileSettingsStackScreen() {
  return (
    <ProfileSettingsStack.Navigator
      screenOptions={{ title: "Настройки профиля" }}
    >
      <ProfileSettingsStack.Screen
        name={routes.PROFILE_SETTINGS_SCREEN}
        component={ProfileSettingsScreen}
      />
      <ProfileSettingsStack.Screen
        name={routes.SCHEDULING}
        component={SchedulingScreen}
        options={{ headerShown: false }}
      />
      <ProfileSettingsStack.Screen
        name={routes.CHOOSE_AREA_WORK}
        component={ChoiceAreasWork}
        options={{ headerShown: false }}
      />
      <ProfileSettingsStack.Screen
        name={routes.PRICE_LIST_SCREEN}
        component={PriceListScreen}
        options={{ title: "Редактировать прайс-лист" }}
      />
    </ProfileSettingsStack.Navigator>
  );
}

function NotificationStackScreen() {
  return (
    <NotificationStack.Navigator screenOptions={{ headerShown: false }}>
      <NotificationStack.Screen
        name="NotificationScreen"
        component={NotificationScreen}
      />
    </NotificationStack.Navigator>
  );
}

export default AppNavigator = () => {
  const isConnected = useIsConnected();

  return (
    <Tab.Navigator screenOptions={{ headerShown: false }}>
      <Tab.Screen
        name={routes.PLANNER_STACK}
        component={PlannerStackScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Icon
              type="material-community"
              name="calendar"
              color={color}
              size={size}
            />
          ),
          title: "Планнер",
        }}
        // listeners={({ navigation, route }) => ({
        //   tabPress: () => {
        //     navigation.navigate(route.name);
        //   },
        // })}
      />
      <Tab.Screen
        name={routes.CLIENTS}
        component={ClientStackScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Icon type="ionicon" name="people" color={color} size={size} />
          ),
          title: "Клиенты",
        }}
        // listeners={({ navigation, route }) => ({
        //   tabPress: () => {
        //     navigation.navigate(route.name);
        //   },
        // })}
      />
      <Tab.Screen
        name={routes.FINANCE_STACK}
        component={FinanceStackScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Icon
              type="material-community"
              name="finance"
              color={color}
              size={size}
            />
          ),
          title: "Финансы",
        }}
        // listeners={({ navigation, route }) => ({
        //   tabPress: () => {
        //     navigation.navigate(route.name);
        //   },
        // })}
      />
      <Tab.Screen
        name="NOTIFICATION_SCREEN"
        component={NotificationStackScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Icon
              type="ionicon"
              name="notifications"
              color={color}
              size={size}
            />
          ),
          tabBarBadge: AppConstants.getNotification() ? "" : undefined,
          tabBarBadgeStyle: {
            height: 12,
            minWidth: 12,
            borderRadius: 6,
          },
          title: "Уведомления",
        }}
      />
      <Tab.Screen
        name={routes.SETTINGS_STACK}
        component={SettingsStackScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Icon
              type="ionicon"
              name="settings-sharp"
              color={color}
              size={size}
            />
          ),
          title: "Настройки",
        }}
        // listeners={({ navigation, route }) => ({
        //   tabPress: () => {
        //     navigation.navigate(route.name);
        //   },
        // })}
      />
    </Tab.Navigator>
  );
};
