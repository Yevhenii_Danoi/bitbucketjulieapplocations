import {
  isCurrentMonth,
  isToday,
  isCurrentYear,
} from "./timePeriodDetermination";

export const todayCount = (financeList, calculationType) => {
  let sum = 0;
  switch (calculationType) {
    case "income": {
      sum = financeList
        .filter((f) => isToday(new Date(f.date)) && f.price > 0)
        .reduce((prev, current) => prev + Number(current.price), 0);
      break;
    }
    case "netProfit": {
      sum =
        financeList
          .filter((f) => isToday(new Date(f.date)) && f.price > 0)
          .reduce((prev, current) => prev + Number(current.price), 0) -
        Math.abs(
          financeList
            .filter((f) => isToday(new Date(f.date)) && f.price < 0)
            .reduce((prev, current) => prev + Number(current.price), 0)
        );
      break;
    }
    case "costs": {
      sum = financeList
        .filter((f) => isToday(new Date(f.date)) && f.price < 0)
        .reduce((prev, current) => prev + Number(current.price), 0);
      break;
    }
    default:
      break;
  }

  return sum.toFixed(2);
};

export const monthlyCount = (financeList, calculationType) => {
  let sum = 0;
  switch (calculationType) {
    case "income": {
      sum = financeList
        .filter((f) => isCurrentMonth(new Date(f.date)) && f.price > 0)
        .reduce((prev, current) => prev + Number(current.price), 0);
      break;
    }
    case "netProfit": {
      sum =
        financeList
          .filter((f) => isCurrentMonth(new Date(f.date)) && f.price > 0)
          .reduce((prev, current) => prev + Number(current.price), 0) -
        Math.abs(
          financeList
            .filter((f) => isCurrentMonth(new Date(f.date)) && f.price < 0)
            .reduce((prev, current) => prev + Number(current.price), 0)
        );
      break;
    }
    case "costs": {
      sum = financeList
        .filter((f) => isCurrentMonth(new Date(f.date)) && f.price < 0)
        .reduce((prev, current) => prev + Number(current.price), 0);
      break;
    }
    default:
      break;
  }
  return sum.toFixed(2);
};

export const yearCount = (financeList, calculationType) => {
  let sum = 0;
  switch (calculationType) {
    case "income": {
      sum = financeList
        .filter((f) => isCurrentYear(new Date(f.date)) && f.price > 0)
        .reduce((prev, current) => prev + Number(current.price), 0);
      break;
    }
    case "netProfit": {
      sum =
        financeList
          .filter((f) => isCurrentYear(new Date(f.date)) && f.price > 0)
          .reduce((prev, current) => prev + Number(current.price), 0) -
        Math.abs(
          financeList
            .filter((f) => isCurrentYear(new Date(f.date)) && f.price < 0)
            .reduce((prev, current) => prev + Number(current.price), 0)
        );
      break;
    }
    case "costs": {
      sum = financeList
        .filter((f) => isCurrentYear(new Date(f.date)) && f.price < 0)
        .reduce((prev, current) => prev + Number(current.price), 0);
      break;
    }
    default:
      break;
  }

  return sum.toFixed(2);
};
