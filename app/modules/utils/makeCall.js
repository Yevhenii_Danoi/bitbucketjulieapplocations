import { Linking } from "react-native";

export const makeCall = (phoneNumber) => {
  let phone = "";
  if (Platform.OS === "android") {
    phone = `tel:${phoneNumber}`;
  } else {
    phone = `telprompt:${phoneNumber}`;
  }

  Linking.openURL(phone);
};
