export const isToday = (someDate) => {
  const today = new Date();
  return (
    someDate.getDate() == today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
  );
};

export const isCurrentMonth = (someDate) => {
  const today = new Date();
  return (
    someDate.getDate() <= today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
  );
};

export const isCurrentYear = (someDate) => {
  const today = new Date();
  return (
    someDate.getDate() <= today.getDate() &&
    someDate.getMonth() <= today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
  );
};

export const stringToDate = (date) => {
  const dataSplit = date.split("-");

  const day = dataSplit[0];
  const month = dataSplit[1];
  const year = dataSplit[2];

  return new Date(year, month - 1, day);
};
