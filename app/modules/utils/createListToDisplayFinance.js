import { monthsNamesInDeclension } from "../../config/calendar";

export default (financeList) => {
  const groupNames = Array.from(
    new Set(
      financeList.map(
        (rec) =>
          rec.date.split("-")[2] +
          " " +
          monthsNamesInDeclension[parseInt(rec.date.split("-")[1] - 1)]
      )
    )
  );

  let groups = {};

  groupNames.forEach((day) => {
    groups[day] = [];
  });

  financeList.forEach((rec) => {
    const day =
      rec.date.split("-")[2] +
      " " +
      monthsNamesInDeclension[parseInt(rec.date.split("-")[1] - 1)];
    groups[day].push(rec);
  });

  let financeArray = [];
  let data = [];
  for (let key in groups) {
    let obj = { title: key };
    for (let record of groups[key]) {
      financeArray.push(record);
    }
    obj["data"] = financeArray;
    data.push(obj);
    financeArray = [];
  }
  return data;
};
