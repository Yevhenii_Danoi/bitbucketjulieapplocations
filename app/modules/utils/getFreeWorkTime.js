// export default getFreeWorkTime = async () => {
//   await getStartEndTimeOfWorkDay(day, formatedDate);
//   await getStartEndTimeOfLunch(day, formatedDate);
//   await getRecordOnThisDate(formatedDate);
// };
import dayjs from "dayjs";

export default dateCercion = (date, leadingDate) => {
  const dateVal = dayjs(leadingDate).toISOString().split("T", 1)[0].split("-");
  date.setYear(dateVal[0]);
  date.setMonth(dateVal[1] - 1);
  date.setDate(dateVal[2]);
  date.setSeconds(0);

  return date;
};
