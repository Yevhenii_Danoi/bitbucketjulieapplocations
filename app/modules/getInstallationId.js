import { Platform } from "react-native";
import * as Application from "expo-application";
import { v5 as uuidv5 } from "uuid";

// used on web. you may need to use a polyfill for
// browsers that do not implement `crypto` interface (for example, IE < 11)
// or, you can use uuid@3.4.0 , which falls back to Math.random()
import { v4 as uuidv4 } from "uuid";

const UUID_NAMESPACE = "29cc8a0d-747c-5f85-9ff9-f2f16636d963";

async function getInstallationIdManually() {
  let installationId;

  if (["android", "ios"].includes(Platform.OS)) {
    let identifierForVendor;

    if (Platform.OS === "android") {
      identifierForVendor = Application.androidId;
    } else {
      // ios
      identifierForVendor = await Application.getIosIdForVendorAsync();
    }

    const bundleIdentifier = Application.applicationId;

    if (identifierForVendor) {
      installationId = uuidv5(
        `${bundleIdentifier}-${identifierForVendor}`,
        UUID_NAMESPACE
      );
    } else {
      const installationTime = await Application.getInstallationTimeAsync();
      installationId = uuidv5(
        `${bundleIdentifier}-${installationTime.getTime()}`,
        UUID_NAMESPACE
      );
    }
  } else {
    // WEB. random (uuid v4)
    installationId = uuidv4();
  }

  return installationId;
}

export default getInstallationIdManually;
