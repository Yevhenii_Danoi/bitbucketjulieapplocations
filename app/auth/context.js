import React, { useState, useEffect } from "react";
import firebase from "firebase";
import { useIsConnected } from "react-native-offline";
import { setCurrentPassword } from "../config/constants";
import { Alert } from "react-native";
import * as AppConstants from "../../app/config/constants";
import * as Notifications from "expo-notifications";
import * as Sentry from "sentry-expo";
import * as Device from "expo-device";

import getInstallationIdManually from "../modules/getInstallationId";

const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const isConnected = useIsConnected();

  const signUp = async (email, password, values) => {
    const { name, surname } = values;
    setIsLoading(true);
    try {
      setError("");
      if (!isConnected) {
        setError("Отсутствует подключение к Интернету");
      } else {
        await firebase.auth().createUserWithEmailAndPassword(email, password);

        await firebase
          .firestore()
          .collection("users")
          .doc(firebase.auth().currentUser.uid)
          .set({
            name,
            surname,
            email,
            isCompletedConfiguration: false,
            smsReminder: `Здравствуйте, [Имя клиента]! Это ${name}, ваш мастер маникюра. Хочу напомнить вам, что вы записаны ко мне на [Дата] на [Время]. Спасибо и хорошего дня!`,
            recordingUrl: `https://julie-614ba.web.app/create-record/${
              firebase.auth().currentUser.uid
            }`,
          });

        try {
          registerForPushNotificationsAsync().then(async (token) => {
            if (token) {
              await AppConstants.addToken(token);

              const pureExpoToken = token.substring(
                token.indexOf("[") + 1,
                token.lastIndexOf("]")
              );

              await firebase
                .firestore()
                .collection("users")
                .doc(firebase.auth().currentUser.uid)
                .update({
                  [`expoTokens.${pureExpoToken}`]: true,
                });

              await firebase
                .firestore()
                .collection("users")
                .doc(firebase.auth().currentUser.uid)
                .collection("notifications")
                .where("isViewed", "==", false)
                .get()
                .then((collection) => {
                  if (collection.docs.length > 0) {
                    AppConstants.setAreNotifications(true);
                  }
                });
            }
          });
        } catch (error) {
          throw new Error("From signUp. ", error);
        }

        setCurrentPassword(password);
      }
    } catch (error) {
      console.log(error);
      setError(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  const updatePassword = async (password) => {
    setError("");
    setIsLoading(true);
    try {
      await firebase.auth().currentUser.updatePassword(password);
      setCurrentPassword(password);
    } catch (error) {
      setError(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  const resetPassword = (email) => {
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        Alert.alert("", "Мы прислали вам ссылку для сброса пароля на почту");
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        Alert.alert("", errorMessage + " Code: " + errorCode);
      });
  };

  const updateEmail = async (email) => {
    setError("");
    try {
      await firebase.auth().currentUser.updateEmail(email);
    } catch (error) {
      setError(error.message);
    }
  };

  const login = async (email, password) => {
    setIsLoading(true);
    try {
      setError("");
      if (!isConnected) {
        setError("Отсутствует подключение к Интернету");
      } else {
        await firebase
          .auth()
          .signInWithEmailAndPassword(email, password)
          .then(async () => {
            try {
              registerForPushNotificationsAsync().then(async (token) => {
                if (token) {
                  await AppConstants.addToken(token);
                  const pureExpoToken = token.substring(
                    token.indexOf("[") + 1,
                    token.lastIndexOf("]")
                  );

                  await firebase
                    .firestore()
                    .collection("users")
                    .doc(firebase.auth().currentUser.uid)
                    .update({
                      [`expoTokens.${pureExpoToken}`]: true,
                    });

                  await firebase
                    .firestore()
                    .collection("users")
                    .doc(firebase.auth().currentUser.uid)
                    .collection("notifications")
                    .where("isViewed", "==", false)
                    .get()
                    .then((collection) => {
                      if (collection.docs.length > 0) {
                        AppConstants.setAreNotifications(true);
                      }
                    });
                }
              });
            } catch (error) {
              throw new Error("From Login. ", error);
            }
          });
        setCurrentPassword(password);
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  const logout = async () => {
    try {
      setError("");

      const currentTokenId = await AppConstants.getCurrentToken();
      const pureExpoToken = currentTokenId.substring(
        currentTokenId.indexOf("[") + 1,
        currentTokenId.lastIndexOf("]")
      );
      await firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .update({
          [`expoTokens.${pureExpoToken}`]: false,
        })
        .then(() => {
          firebase.auth().signOut();
        });
    } catch (error) {
      setError(error);
    }
  };

  async function registerForPushNotificationsAsync() {
    let token;
    if (Device.isDevice) {
      const { status: existingStatus } =
        await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Не удалось получить жетон для push-уведомления!");
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
    } else {
      alert(
        "Необходимо использовать физическое устройство для push-уведомлений"
      );
    }

    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }

    return token;
  }

  const authContext = {
    signUp,
    login,
    logout,
    updatePassword,
    updateEmail,
    error,
    setError,
    isLoading,
    resetPassword,
  };

  return (
    <AuthContext.Provider value={authContext}>{children}</AuthContext.Provider>
  );
};

export default AuthContext;
