import * as Updates from "expo-updates";

const settings = {
  dev: {
    firebaseConfig: {
      apiKey: "AIzaSyBh5qD7TavTcxMyrGPCWp_PgGkH71xv-fA",
      authDomain: "julie-development-70647.firebaseapp.com",
      projectId: "julie-development-70647",
      storageBucket: "julie-development-70647.appspot.com",
      messagingSenderId: "531758065369",
      appId: "1:531758065369:web:58d83905f1a59effbb00c8",
      measurementId: "G-BWL117QGML",
    },
  },
  staging: {
    firebaseConfig: {
      apiKey: "AIzaSyBh5qD7TavTcxMyrGPCWp_PgGkH71xv-fA",
      authDomain: "julie-development-70647.firebaseapp.com",
      projectId: "julie-development-70647",
      storageBucket: "julie-development-70647.appspot.com",
      messagingSenderId: "531758065369",
      appId: "1:531758065369:web:58d83905f1a59effbb00c8",
      measurementId: "G-BWL117QGML",
    },
  },
  prod: {
    firebaseConfig: {
      apiKey: "AIzaSyADk5PHI3neGHaNwEHbP0R9OLw-uLGcJCY",
      authDomain: "julie-614ba.firebaseapp.com",
      projectId: "julie-614ba",
      storageBucket: "julie-614ba.appspot.com",
      messagingSenderId: "34131280099",
      appId: "1:34131280099:web:c681ef114f418ede2185d6",
      measurementId: "G-8M04YSMFNK",
    },
  },
};

const getCurrentSettings = () => {
  if (Updates.releaseChannel.startsWith("prod")) return settings.prod;
  if (Updates.releaseChannel.startsWith("staging")) return settings.staging;
  return settings.dev;
};

export default getCurrentSettings();
