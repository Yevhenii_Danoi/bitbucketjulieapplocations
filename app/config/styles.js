import { Platform } from "react-native";
import colors from "./colors";
import theme from "./theme";

export default {
  colors,
  theme,
  mainText: {
    color: colors.grey,
    fontSize: 32,
    fontWeight: "400",
    fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
    marginBottom: 15,
  },
  regularText: {
    color: colors.medium,
    fontSize: 16,
    textAlign: "center",
    lineHeight: 30,
    margin: 10,
    paddingHorizontal: 15,
  },
};
