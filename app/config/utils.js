export const getCapitalLetters = (fullname) => {
  return fullname
    ? fullname
        .split(" ")
        .map((item) => item[0])
        .join("")
        .substring(0, 2)
    : "";
};
