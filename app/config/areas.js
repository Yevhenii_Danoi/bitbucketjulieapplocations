export default [
  {
    id: 1,
    area: "Маникюр",
    priceList: [
      { id: 1, procedure: "Педикюр", price: 140, duration: 122 },
      { id: 2, procedure: "Маникюр", price: 1450, duration: 10 },
      { id: 3, procedure: "Наращивание", price: 120, duration: 40 },
      { id: 4, procedure: "Лак", price: 142, duration: 30 },
    ],
  },
  {
    id: 2,
    area: "Педикюр",
    priceList: [
      { id: 1, procedure: "Педикюр", price: 140, duration: 122 },
      { id: 2, procedure: "Маникюр", price: 1450, duration: 10 },
      { id: 3, procedure: "Наращивание", price: 120, duration: 40 },
      { id: 4, procedure: "Лак", price: 142, duration: 30 },
    ],
  },
  {
    id: 3,
    area: "Брови",
    priceList: [
      { id: 1, procedure: "Педикюр", price: 140, duration: 122 },
      { id: 2, procedure: "Маникюр", price: 1450, duration: 10 },
      { id: 3, procedure: "Наращивание", price: 120, duration: 40 },
      { id: 4, procedure: "Лак", price: 142, duration: 30 },
    ],
  },
  {
    id: 4,
    area: "Массаж",
    priceList: [
      { id: 1, procedure: "Педикюр", price: 140, duration: 122 },
      { id: 2, procedure: "Маникюр", price: 1450, duration: 10 },
      { id: 3, procedure: "Наращивание", price: 120, duration: 40 },
      { id: 4, procedure: "Лак", price: 142, duration: 30 },
    ],
  },
  {
    id: 5,
    area: "Массаж",
    priceList: [
      { id: 1, procedure: "Педикюр", price: 140, duration: 122 },
      { id: 2, procedure: "Маникюр", price: 1450, duration: 10 },
      { id: 3, procedure: "Наращивание", price: 120, duration: 40 },
      { id: 4, procedure: "Лак", price: 142, duration: 30 },
    ],
  },
  {
    id: 6,
    area: "Массаж",
    priceList: [
      { id: 1, procedure: "Педикюр", price: 140, duration: 122 },
      { id: 2, procedure: "Маникюр", price: 1450, duration: 10 },
      { id: 3, procedure: "Наращивание", price: 120, duration: 40 },
      { id: 4, procedure: "Лак", price: 142, duration: 30 },
    ],
  },
  {
    id: 7,
    area: "Массаж",
    priceList: [
      { id: 1, procedure: "Педикюр", price: 140, duration: 122 },
      { id: 2, procedure: "Маникюр", price: 1450, duration: 10 },
      { id: 3, procedure: "Наращивание", price: 120, duration: 40 },
      { id: 4, procedure: "Лак", price: 142, duration: 30 },
    ],
  },
];
