import getInstallationIdManually from "../modules/getInstallationId";
import * as SecureStore from "expo-secure-store";

let currentPassword = "";
let notification = false;

export const addButtonSize = 50;

export const setCurrentPassword = (password) => {
  SecureStore.setItemAsync("password", password);
};

export const getCurrentPassword = () => {
  return SecureStore.getItemAsync("password");
};

export const setAreNotifications = (bool) => {
  notification = bool;
};

export const getNotification = () => {
  return notification;
};

export const addToken = async (expoToken) => {
  await getInstallationIdManually().then((installationId) => {
    SecureStore.setItemAsync(installationId, expoToken);
  });
};

export const getCurrentToken = async () => {
  const installationId = await getInstallationIdManually();
  return SecureStore.getItemAsync(installationId);
};
