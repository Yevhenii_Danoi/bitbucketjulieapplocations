export default {
  black: "#000",
  lightblack: "#171c21",
  danger: "#ff5252",
  dark: "#0c0c0c",
  light: "#f8f4f4",
  medium: "#6e6969",
  primary: "#fc5c65",
  secondary: "#4ecdc4",
  green: "#04b843",
  gray: "gray",
  white: "#fff",
};
