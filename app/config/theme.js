export default {
  Button: {
    titleStyle: {
      fontSize: 14,
      color: "#fff",
      letterSpacing: 1.3,
      textTransform: "uppercase",
      width: "90%",
    },
    buttonStyle: {
      backgroundColor: "#333",
      borderRadius: 10,
      marginBottom: 15,
      paddingVertical: 15,
    },
  },
};
