import React from "react";
import { StyleSheet, View } from "react-native";

export default function ExecutionLine({ color = "#fff" }) {
  return (
    <View
      style={{
        flex: 1,
        height: 5,
        backgroundColor: color,
      }}
    />
  );
}

const styles = StyleSheet.create({});
