import React, { useState, useEffect } from "react";
import { StyleSheet, Text, TextInput, Keyboard } from "react-native";

import defaultStyle from "../config/styles";

export default function TextInputCard({
  label,
  onChangeText,
  onBlur = (f) => f,
  style,
  ...otherProps
}) {
  const [onFocus, setOnFocus] = useState(false);
  const [borderColor, setBorderColor] = useState(
    onFocus ? defaultStyle.colors.secondary : defaultStyle.colors.gray
  );

  useEffect(() => {
    setBorderColor(
      onFocus ? defaultStyle.colors.secondary : defaultStyle.colors.gray
    );
  }, [onFocus]);

  return (
    <>
      <TextInput
        onFocus={() => {
          setOnFocus(true);
        }}
        onBlur={() => {
          setOnFocus(false);
          onBlur();
        }}
        onChangeText={(text) => onChangeText(text)}
        style={[styles.textInput, { borderColor: borderColor }, style]}
        {...otherProps}
      />
      <Text style={styles.text}>{label}</Text>
    </>
  );
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    height: 40,
    borderWidth: 0,
    borderBottomWidth: 3,
    marginBottom: 7,
  },
  text: {
    color: defaultStyle.colors.medium,
    letterSpacing: 0.8,
    marginBottom: 10,
  },
});
