import React, { useState, useEffect } from "react";
import { StyleSheet, FlatList, Text } from "react-native";
import firebase from "firebase";

import defaultStyle from "../config/styles";
import ClientCard from "./ClientCard";
import { SafeAreaView } from "react-native-safe-area-context";
import ProjectActivityIndicator from "./ProjectActivityIndicator";
import NoDataContainer from "./NoDataContainer";
import BackgroundPicture from "./BackgroundPicture";
("../components/NoDataContainer");

export default function ClientCardList({ date, navigation, animation }) {
  const [allClients, setAllClients] = useState([]);
  const [regularClients, setRegularClients] = useState([]);

  const [regularRecords, setRegularRecords] = useState([]);

  const [isRegularLoaded, setIsRegularLoaded] = useState(false);

  const userDoc = firebase
    .firestore()
    .collection("users")
    .doc(firebase.auth().currentUser.uid);

  useEffect(() => {
    setIsRegularLoaded(false);
    const unsubscribe = userDoc
      .collection("records")
      .where("date", "==", date)
      .onSnapshot((snapshot) => {
        if (snapshot.size) {
          const data = snapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
          }));
          setRegularRecords(data);
        } else {
          setRegularRecords([]);
        }
      });

    return () => unsubscribe();
  }, [date]);

  useEffect(() => {
    let getClients = (record) => {
      return new Promise(async (resolve) => {
        const dbClientPromise = userDoc
          .collection("clients")
          .doc(record.clientID)
          .get();

        const dbPromise = userDoc
          .collection("areas")
          .doc(record.areaID)
          .collection("price-list")
          .doc(record.procedureID)
          .get();

        const recSnapshot = await dbPromise;
        const clientSnapshot = await dbClientPromise;
        resolve({
          ...clientSnapshot.data(),
          date: record.date,
          clientID: record.clientID,
          recordID: record.id,
          isDone: record.isDone,
          fromOnline: record.fromOnline,
          lastModified: record.lastModified,
          startTime: record.startTime,
          endTime: record.endTime,
          ...recSnapshot.data(),
        });
      });
    };

    let actions = regularRecords.map(getClients);

    if (regularRecords.length <= 0) {
      setRegularClients([]);
    } else {
      Promise.all(actions).then((results) => {
        setRegularClients(results);
      });
    }
  }, [regularRecords]);

  useEffect(() => {
    setIsRegularLoaded(true);
  }, [regularClients]);

  useEffect(() => {
    if (regularClients) setAllClients(regularClients);
  }, [regularClients]);

  if (isRegularLoaded && allClients.length > 0)
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          initialNumToRender={10}
          data={allClients}
          keyExtractor={(item) => item.recordID.toString()}
          renderItem={({ item }) => (
            <ClientCard
              client={item}
              navigation={navigation}
              animation={animation}
            />
          )}
        />
      </SafeAreaView>
    );

  if (isRegularLoaded && allClients.length <= 0) {
    return (
      <BackgroundPicture
        source={require("../assets/images/manicure-client.png")}
      />
    );
  }

  return <ProjectActivityIndicator />;
}

const styles = StyleSheet.create({
  cardContainer: {
    margin: 0,
    borderRadius: 10,
    backgroundColor: defaultStyle.colors.light,
  },
  container: {
    flex: 1,
    paddingHorizontal: 15,
  },
});
