import React from "react";
import { StyleSheet, Text } from "react-native";
import { Overlay } from "react-native-elements";
import ProjectActivityIndicator from "../components/ProjectActivityIndicator";

export default function DeleteOverlay() {
  return (
    <Overlay
      isVisible={true}
      overlayStyle={{
        maxWidth: "90%",
        paddingHorizontal: 40,
        paddingVertical: 20,
        borderRadius: 0,
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <ProjectActivityIndicator
        style={{ marginRight: 20, flex: 0 }}
        size={30}
      />
      <Text style={{ fontSize: 16, letterSpacing: 0.9 }}>
        Минутку. Идёт удаление...
      </Text>
    </Overlay>
  );
}

const styles = StyleSheet.create({});
