import React from "react";
import { StyleSheet, Text, View } from "react-native";
import defaultStyle from "../config/styles";
import Constants from "expo-constants";
import { useNetInfo } from "@react-native-community/netinfo";

export default function OfflineNotice() {
  const netInfo = useNetInfo();
  if (netInfo.type !== "unknown" && netInfo.isInternetReachable === false)
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Нет Интернет-соединения</Text>
        <Text style={[styles.text, { fontSize: 10 }]}>
          Могут отображаться не все данные
        </Text>
      </View>
    );

  return null;
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: defaultStyle.colors.lightblack,
    height: 50,
    width: "100%",
    position: "absolute",
    top: Constants.statusBarHeight,
    zIndex: 1000,
  },
  text: {
    color: defaultStyle.colors.white,
  },
});
