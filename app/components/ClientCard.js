import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, Pressable, Alert } from "react-native";
import { Card, Icon, Avatar, Overlay, Badge } from "react-native-elements";
import firebase from "firebase";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { Swipeable } from "react-native-gesture-handler";

import defaultStyle from "../config/styles";
import { getCapitalLetters } from "../config/utils";
import routes from "../navigation/routes";
import OrderCompletedButton from "./OrderCompletedButton";
import ProjectActivityIndicator from "./ProjectActivityIndicator";
import DeleteOverlay from "./DeleteOverlay";

export default function ClientCard({ client, navigation, animation }) {
  const {
    name = "Неизвестный",
    avatar = "null",
    procedure,
    hours,
    minutes,
    isDone,
    price,
    date: procedureDate,
    recordID,
    clientID,
    color,
    fromOnline,
  } = client;
  const [isDoneMark, setIsDoneMark] = useState(isDone);
  const [buttonColor, setButtonColor] = useState(
    isDoneMark ? defaultStyle.colors.green : defaultStyle.colors.medium
  );
  const [date, setDate] = useState(new Date(procedureDate));
  const [isDeleting, setIsDeleting] = useState(false);

  const userDoc = firebase
    .firestore()
    .collection("users")
    .doc(firebase.auth().currentUser.uid);

  useEffect(() => {
    setButtonColor(
      isDoneMark ? defaultStyle.colors.green : defaultStyle.colors.medium
    );
  }, [isDoneMark]);

  const newFinance = {
    name: name,
    category: procedure,
    date: new Date().toISOString().split("T")[0],
    price: price,
    recordID: recordID,
  };

  const onDeleteRecord = () => {
    setIsDeleting(true);
    const unsubscribe = firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("records")
      .doc(recordID)
      .delete()
      .catch((error) => {
        Alert.alert(
          "Произошла ошибка",
          "Приносим извинения. Произошла ошибка удаления записи. Попробуйте еще разок"
        );
        console.log("Ошибка удаления записи", error);
      })
      .finally(() => {
        setIsDeleting(false);
      });

    return () => unsubscribe();
  };

  const removeOrMarkAsDone = () => {
    if (isDoneMark) handleChangeStatus(true);
    else handleChangeStatus(false);
  };

  const handleChangeStatus = async (isDone) => {
    setIsDoneMark(!isDone);
    if (!isDone) {
      await userDoc
        .collection("finance")
        .add(newFinance)
        .catch((error) => console.error("Line 88", error));
    } else {
      await userDoc
        .collection("finance")
        .where("recordID", "==", recordID)
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            doc.ref.delete();
          });
        })
        .catch((error) => console.error("Line 95. \n", error));
    }

    await userDoc
      .collection("records")
      .doc(recordID)
      .update({
        isDone: !isDone,
      })
      .catch((error) => {
        setIsDoneMark(!isDone);
        Alert.alert("Что-то пошло не так...");
        console.error("Line 98.", error);
      });

    if (!isDone) {
      await userDoc
        .collection("clients")
        .doc(clientID)
        .get()
        .then((doc) => {
          let visit = doc.data().lastVisit;

          if (new Date(date) > new Date(visit) || visit === "не был")
            visit = new Date(date.getTime() - date.getTimezoneOffset() * 60000)
              .toISOString()
              .split("T")[0];
          return visit;
        })
        .then((visit) => {
          updateLastVisit(visit);
        });
    } else {
      await userDoc
        .collection("records")
        .where("clientID", "==", clientID)
        .where("isDone", "==", true)
        .orderBy("date", "desc")
        .get()
        .then((recs) => {
          if (recs.docs.length <= 0) {
            return "не был";
          } else return recs.docs[0].data().date;
        })
        .then((visit) => {
          updateLastVisit(visit);
        });
    }
  };

  const updateLastVisit = (visit) => {
    userDoc
      .collection("clients")
      .doc(clientID)
      .update({
        lastVisit: visit,
      })
      .catch((error) =>
        console.error("Cannot update lastVisit. Line 109", error)
      );
  };

  const showAlert = () => {
    Alert.alert(
      "Удалить запись?",
      "Это действие невозможно отменить. На вкладке Финансы не произойдет изменений",
      [
        {
          text: "Отмена",
          style: "cancel",
        },
        { text: "OK", onPress: () => onDeleteRecord() },
      ]
    );
  };

  const renderRightActions = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
      extrapolate: "clamp",
    });
    return (
      <OrderCompletedButton
        action={removeOrMarkAsDone}
        animation={!isDoneMark ? animation : null}
        backgroundColor={buttonColor}
        icon={isDoneMark ? "done-all" : "done"}
        scale={scale}
        text={isDoneMark ? "Сделано" : "Не сделано"}
      />
    );
  };
  return (
    <GestureHandlerRootView>
      {isDeleting && <DeleteOverlay />}
      <Pressable
        onPress={() =>
          navigation.navigate(routes.ABOUT_CLIENT_RECORD, { client })
        }
        onLongPress={showAlert}
      >
        <Swipeable renderRightActions={renderRightActions}>
          <Card containerStyle={styles.cardContainer}>
            <View style={styles.container}>
              <View style={styles.contentContainer}>
                <View style={[styles.alignment, { marginBottom: 12 }]}>
                  <Avatar
                    containerStyle={{ backgroundColor: color }}
                    title={getCapitalLetters(name.toString())}
                    titleStyle={styles.titleAvatar}
                    rounded
                    size={55}
                    // source={{ uri: avatar || "null" }}
                  />
                  {fromOnline && (
                    <Badge
                      status="success"
                      badgeStyle={{ width: 15, height: 15, borderRadius: 10 }}
                      containerStyle={{
                        position: "absolute",
                        top: 0,
                        left: 35,
                      }}
                    />
                  )}
                  <Text style={styles.nameText}>{name}</Text>
                </View>

                <View style={styles.alignment}>
                  <View style={[styles.iconView, styles.flex_1]}>
                    <Icon
                      type="material-community"
                      name="clock-time-four-outline"
                      size={20}
                      style={styles.icon}
                    />
                    <Text>
                      {hours} час(а) {minutes} минут
                    </Text>
                  </View>

                  <View style={[styles.iconView, styles.flex_2]}>
                    <Icon
                      type="foundation"
                      name="clipboard-notes"
                      size={20}
                      style={styles.icon}
                    />
                    <Text
                      style={styles.text}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {procedure}
                    </Text>
                  </View>
                </View>

                <View style={[styles.iconView, styles.flex_2]}>
                  <Icon
                    type="ionicon"
                    name="pricetag"
                    size={20}
                    style={styles.icon}
                  />
                  <Text
                    style={styles.text}
                    numberOfLines={1}
                    ellipsizeMode="tail"
                  >
                    {price} грн.
                  </Text>
                </View>
              </View>
              <View
                style={[
                  styles.progressStripe,
                  {
                    backgroundColor: buttonColor,
                  },
                ]}
              ></View>
            </View>
          </Card>
        </Swipeable>
      </Pressable>
    </GestureHandlerRootView>
  );
}

const styles = StyleSheet.create({
  alignment: {
    flexDirection: "row",
    alignItems: "center",
  },
  avatarContainer: {
    marginRight: 15,
  },
  cardContainer: {
    backgroundColor: defaultStyle.colors.light,
    borderRadius: 10,
    borderRightWidth: 0,
    margin: 0,
    marginBottom: 15,
    padding: 0,
  },
  container: {
    flex: 1,
    flexDirection: "row",
  },
  contentContainer: {
    flex: 1,
    padding: 15,
  },
  flex_1: {
    flex: 1,
  },
  flex_2: {
    flex: 2,
  },
  progressStripe: {
    width: 5,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  icon: {
    marginRight: 5,
  },
  iconView: {
    flexDirection: "row",
    alignItems: "center",
    margin: 5,
  },
  nameText: {
    fontSize: 22,
    letterSpacing: 1.1,
    marginLeft: 15,
  },
  titleAvatar: {
    fontSize: 20,
  },
  text: {
    flex: 1,
  },
});
