import React, { useEffect, useState } from "react";
import {
  Image,
  StyleSheet,
  ActivityIndicator,
  Text,
  FlatList,
} from "react-native";
import { ListItem } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import firebase from "firebase";
import ProjectActivityIndicator from "./ProjectActivityIndicator";

import defaultStyles from "../config/styles";
import { monthsNamesInDeclension } from "../config/calendar";
import * as AppConstants from "../config/constants";
import BackgroundPicture from "./BackgroundPicture";

export default function NotificationScreen({ navigation }) {
  const [notifications, setNotifications] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.getParent().addListener("focus", () => {
      AppConstants.setAreNotifications(false);
      firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .collection("notifications")
        .where("isViewed", "==", false)
        .get()
        .then((snapshot) =>
          snapshot.docs.forEach((doc) => {
            firebase
              .firestore()
              .collection("users")
              .doc(firebase.auth().currentUser.uid)
              .collection("notifications")
              .doc(doc.id)
              .update({
                isViewed: true,
              });
          })
        );
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("notifications")
      .orderBy("date", "desc")
      .onSnapshot((snapshot) => {
        const data = snapshot.docs.map((doc) => ({
          id: doc.id,
          sentTime: `${new Date(doc.data().date.seconds * 1000).getDate()} ${
            monthsNamesInDeclension[
              new Date(doc.data().date.seconds * 1000).getMonth()
            ]
          }, ${new Date(doc.data().date.seconds * 1000).getHours()}:${new Date(
            doc.data().date.seconds * 1000
          ).getMinutes()} `,
          ...doc.data(),
        }));
        setNotifications(data);
        setIsLoaded(true);
      });
  }, []);

  if (isLoaded && notifications.length === 0)
    return (
      <BackgroundPicture source={require("../assets/images/appointment.png")}>
        <Text style={defaultStyles.regularText}>
          Здесь появятся уведомления об онлайн-записях. Разместите ссылку на
          страничку онлайн-записи в Instagram (Смотрите настройки профиля)
        </Text>
      </BackgroundPicture>
    );

  if (isLoaded && notifications.length !== 0)
    return (
      <SafeAreaView>
        <FlatList
          data={notifications}
          keyExtractor={(notification) => notification.id.toString()}
          renderItem={({ item }) => (
            <ListItem bottomDivider>
              <ListItem.Content style={styles.content}>
                <Text style={styles.sentTimeStlye}>{item.sentTime}</Text>
                <ListItem.Title style={styles.message}>
                  {item.message}
                </ListItem.Title>
              </ListItem.Content>
            </ListItem>
          )}
        />
      </SafeAreaView>
    );

  return <ProjectActivityIndicator />;
}

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  message: {
    lineHeight: 30,
  },
  sentTimeStlye: {
    alignSelf: "flex-end",
    marginBottom: 5,
    fontWeight: "700",
  },
});
