import React from "react";
import { StyleSheet, Image, ActivityIndicator } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

export default function BackgroundPicture(props) {
  return (
    <SafeAreaView style={styles.container}>
      <Image
        style={styles.image}
        source={props.source}
        PlaceholderContent={<ActivityIndicator size="large" />}
      />
      {props.children}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 25,
  },
  image: {
    height: 250,
    resizeMode: "contain",
  },
});
