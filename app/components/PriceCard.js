import React, { useState, useEffect } from "react";
import { Pressable, StyleSheet, View, Text, Alert } from "react-native";
import { Card, Icon } from "react-native-elements";
import DateTimePickerResult from "@react-native-community/datetimepicker";
import dayjs from "dayjs";

import TextInputCard from "./TextInputCard";
import defaultStyle from "../config/styles";

export default function PriceCard({
  onChangeState = (f) => f,
  value,
  deleteCard = (f) => f,
}) {
  const [procedure, onProcedureChange] = useState();
  const [price, onPriceChange] = useState();

  //Для времени
  const today = new Date();
  const [showPicker, setShowPicker] = useState(false);
  const [displayProcedureDuration, setDisplayProcedureDuration] = useState();
  const [procedureDuration, setProcedureDuration] = useState();

  const [updateDBDur, setUpdateDBDur] = useState(false);

  useEffect(() => {
    today.setHours(value.hours, value.minutes);
    setProcedureDuration(today);
    onProcedureChange(value.procedure);
    onPriceChange(value.price.toString());
  }, []);

  useEffect(() => {
    if (updateDBDur) onBlur();
  }, [updateDBDur]);

  useEffect(() => {
    setDisplayProcedureDuration(
      `${dayjs(procedureDuration).get("hour")} час(а) ${dayjs(
        procedureDuration
      ).get("minute")} минут`
    );
  }, [procedureDuration]);

  const onProcedureDurationChange = (event, selectedDuration) => {
    setShowPicker(Platform.OS === "ios");
    if (event.type === "set") {
      const currentDuration = selectedDuration;
      setProcedureDuration(currentDuration);
      setUpdateDBDur(true);
    }
  };

  const onBlur = () => {
    const changeObj = {
      id: value.id,
      procedure: procedure,
      price: Math.abs(Number.parseFloat(price.replace(",", ".")).toFixed(2)),
      hours: dayjs(procedureDuration).get("hour"),
      minutes: dayjs(procedureDuration).get("minute"),
    };
    onChangeState(changeObj);
    setUpdateDBDur(false);
  };

  const showTimePicker = () => {
    setShowPicker(true);
  };

  return (
    <Card containerStyle={styles.card}>
      <Pressable
        onPress={() => {
          deleteCard(value);
        }}
      >
        <Icon type="ant-design" name="close" style={styles.icon} size={20} />
      </Pressable>
      <Pressable>
        <TextInputCard
          label="Услуга"
          onBlur={() => onBlur()}
          onChangeText={onProcedureChange}
          value={procedure}
        />
      </Pressable>
      <Pressable>
        <TextInputCard
          label="Стоимость"
          onChangeText={onPriceChange}
          onBlur={() => onBlur()}
          value={price}
          keyboardType="numeric"
        />
      </Pressable>
      <Pressable onPress={showTimePicker}>
        <View>
          <Text>{displayProcedureDuration}</Text>
          <Text style={styles.text}>Продолжительность</Text>
        </View>
      </Pressable>
      {showPicker && (
        <DateTimePickerResult
          testID="timePicker"
          value={procedureDuration}
          mode="time"
          is24Hour={true}
          display="spinner"
          onChange={onProcedureDurationChange}
        />
      )}
    </Card>
  );
}

const styles = StyleSheet.create({
  card: {
    margin: 0,
    marginBottom: 20,
  },
  durationContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
  },
  hourInput: {
    marginRight: 10,
    textAlign: "center",
  },
  icon: {
    alignSelf: "flex-end",
  },
  minuteInput: {
    marginLeft: 10,
    textAlign: "center",
  },
  timeSeperator: {
    alignSelf: "center",
    color: defaultStyle.colors.black,
    fontWeight: "900",
    fontSize: 20,
  },
  text: {
    color: defaultStyle.colors.medium,
    letterSpacing: 0.8,
  },
});
