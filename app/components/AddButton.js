import React from "react";
import { Pressable, StyleSheet, Image } from "react-native";
import { Icon, SpeedDial } from "react-native-elements";

import { addButtonSize } from "../config/constants";

export default function AddButton({ onPress }) {
  return (
    <SpeedDial
      color="transparent"
      icon={<Icon type="antdesign" name="plus" color="#fff" />}
      openIcon={<Icon type="antdesign" name="close" color="#fff" />}
      onOpen={onPress}
      overlayColor="rgba(255,255,255,0)"
      containerStyle={{
        backgroundColor: "#333",
      }}
    />
  );
}

const styles = StyleSheet.create({});
