import React from "react";
import { Pressable, Animated, StyleSheet } from "react-native";
import { Icon } from "react-native-elements";

import defaultStyle from "../config/styles";

export default function OrderCompletedButton({
  action,
  animation,
  backgroundColor,
  icon,
  scale,
  text,
}) {
  return (
    <Pressable
      style={[
        styles.rightActionContainer,
        {
          backgroundColor: backgroundColor,
        },
      ]}
      onPress={() => {
        action();
        if (animation) animation();
      }}
    >
      <Icon
        type="material-icon"
        name={icon}
        color={defaultStyle.colors.white}
        size={30}
      />
      <Animated.Text
        style={[styles.actionText, { transform: [{ translateX: scale }] }]}
      >
        {text}
      </Animated.Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  actionText: {
    color: "#fff",
    fontWeight: "600",
    paddingHorizontal: 20,
    textAlign: "center",
  },
  rightActionContainer: {
    width: 110,
    alignItems: "center",
    borderRadius: 5,
    justifyContent: "center",
    marginBottom: 15,
    marginRight: 5,
    marginLeft: -10,
  },
});
