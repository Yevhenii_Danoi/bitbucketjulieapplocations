import React from "react";
import { StyleSheet } from "react-native";
import { Overlay } from "react-native-elements";

import ProjectActivityIndicator from "./ProjectActivityIndicator";

export default function OverlayAI({ visible }) {
  return (
    <Overlay
      isVisible={visible}
      fullScreen={true}
      overlayStyle={styles.overlay}
      backdropStyle={styles.backdrop}
    >
      <ProjectActivityIndicator />
    </Overlay>
  );
}

const styles = StyleSheet.create({
  backdrop: {
    opacity: 0,
  },
  overlay: {
    opacity: 0.7,
  },
});
