import React from "react";
import { StyleSheet } from "react-native";
import { Input } from "react-native-elements";

import defaultStyle from "../../app/config/styles";

export default function TextInput({ ...otherProps }) {
  return (
    <Input
      placeholderTextColor={defaultStyle.colors.medium}
      containerStyle={{ paddingHorizontal: 0 }}
      inputContainerStyle={styles.input}
      {...otherProps}
    />
  );
}

const styles = StyleSheet.create({
  input: {
    borderColor: defaultStyle.colors.medium,
    borderRadius: 10,
    borderWidth: 1,
    marginTop: 5,
    fontSize: 18,
    padding: 15,
    height: 60,
  },
});
