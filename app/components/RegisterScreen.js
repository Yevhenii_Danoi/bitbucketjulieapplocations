import React from "react";
import { ScrollView, StyleSheet, View } from "react-native";

import UnderDivider from "./UnderDivider";

export default function RegisterScreen({
  children,
  buttonTitle,
  onPress,
  authScreen = false,
}) {
  return (
    <ScrollView
      contentContainerStyle={styles.scrollView}
      keyboardShouldPersistTaps="always"
    >
      <View style={styles.aboveDivider}>{children}</View>
      <UnderDivider
        buttonTitle={buttonTitle}
        onPress={onPress}
        icon="arrow-right"
        authScreen={authScreen}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  aboveDivider: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  scrollView: {
    flexGrow: 1,
  },
});
