import React, { useState, useRef } from "react";
import { StyleSheet } from "react-native";
import { Picker } from "@react-native-picker/picker";

import defaultStyles from "../../config/styles";

export default function TimePicker({ onChange, startValue, enabled }) {
  const [selectedTime, setSelectedTime] = useState(startValue);
  const pickerRef = useRef();

  function open() {
    pickerRef.current.focus();
  }

  function close() {
    pickerRef.current.blur();
  }

  return (
    <Picker
      ref={pickerRef}
      selectedValue={selectedTime}
      onValueChange={(itemValue, itemIndex) => {
        onChange(itemValue);
        setSelectedTime(itemValue);
      }}
      style={styles.pickerContainer}
      enabled={enabled}
    >
      <Picker.Item label="6:30" value="6:30" />
      <Picker.Item label="7:00" value="7:00" />
      <Picker.Item label="7:30" value="7:30" />
      <Picker.Item label="8:00" value="8:00" />
      <Picker.Item label="8:30" value="8:30" />
      <Picker.Item label="9:00" value="9:00" />
      <Picker.Item label="9:30" value="9:30" />
      <Picker.Item label="10:00" value="10:00" />
      <Picker.Item label="10:30" value="10:30" />
      <Picker.Item label="11:00" value="11:00" />
      <Picker.Item label="11:30" value="11:30" />
      <Picker.Item label="12:00" value="12:00" />
      <Picker.Item label="12:30" value="12:30" />
      <Picker.Item label="13:00" value="13:00" />
      <Picker.Item label="13:30" value="13:30" />
      <Picker.Item label="14:00" value="14:00" />
      <Picker.Item label="14:30" value="14:30" />
      <Picker.Item label="15:00" value="15:00" />
      <Picker.Item label="15:30" value="15:30" />
      <Picker.Item label="16:00" value="16:00" />
      <Picker.Item label="16:30" value="16:30" />
      <Picker.Item label="17:00" value="17:00" />
      <Picker.Item label="17:30" value="17:30" />
      <Picker.Item label="18:00" value="18:00" />
      <Picker.Item label="18:30" value="18:30" />
      <Picker.Item label="19:00" value="19:00" />
      <Picker.Item label="19:30" value="19:30" />
      <Picker.Item label="20:00" value="20:00" />
      <Picker.Item label="20:30" value="20:30" />
      <Picker.Item label="21:00" value="21:00" />
      <Picker.Item label="21:30" value="21:30" />
      <Picker.Item label="22:00" value="22:00" />
    </Picker>
  );
}

const styles = StyleSheet.create({
  pickerContainer: {
    flex: 1,
    paddingVertical: 25,
  },
});
