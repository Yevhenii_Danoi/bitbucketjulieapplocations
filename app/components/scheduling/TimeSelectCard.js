import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card, Button, CheckBox, ThemeProvider } from "react-native-elements";

import TimePicker from "./TimePicker";
import defaultStyle from "../../config/styles";
import { SafeAreaView } from "react-native-safe-area-context";
import theme from "../../config/theme";

export default function TimeSelectCard({ onPress, dayName }) {
  const [checked, setChecked] = useState(false);

  const [startWorkTime, setStartWorkTime] = useState("8:00");
  const [endWorkTime, setEndWorkTime] = useState("17:00");
  const [startLunchTime, setStartLunchime] = useState("12:00");
  const [endLunchTime, setEndLunchTime] = useState("12:30");

  const saveAndBack = () => {
    if (startWorkTime && endWorkTime) {
      if (checked)
        onPress(startWorkTime, endWorkTime, startLunchTime, endLunchTime);
      else onPress(startWorkTime, endWorkTime, null, null);
    } else onPress();
  };
  return (
    <ThemeProvider theme={theme}>
      <SafeAreaView style={styles.mainContainer}>
        <Card>
          <Card.Title style={styles.header}>{dayName}</Card.Title>
          <Card.Divider />
          <Text style={styles.subtitle}>Рабочее время</Text>
          <View style={styles.compositeContainer}>
            <TimePicker
              onChange={(time) => {
                setStartWorkTime(time);
              }}
              startValue="8:00"
            />
            <Text style={styles.text}>–</Text>
            <TimePicker
              onChange={(time) => {
                setEndWorkTime(time);
              }}
              startValue="17:00"
            />
          </View>

          <View style={styles.compositeContainer}>
            <Text style={styles.subtitle}>Обеденный перерыв</Text>
            <CheckBox
              checkedColor={defaultStyle.colors.black}
              checked={checked}
              onPress={() => setChecked(!checked)}
            />
          </View>
          <View style={styles.compositeContainer}>
            <TimePicker
              onChange={(time) => {
                setStartLunchime(time);
              }}
              startValue="12:00"
              enabled={checked}
            />
            <Text style={styles.text}>–</Text>
            <TimePicker
              onChange={(time) => {
                setEndLunchTime(time);
              }}
              startValue="12:30"
              enabled={checked}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Button
              containerStyle={[styles.button, { paddingRight: 10 }]}
              buttonStyle={{
                marginBottom: 0,
              }}
              title="Отмена"
              onPress={onPress}
            />
            <Button
              containerStyle={[styles.button, { paddingLeft: 10 }]}
              buttonStyle={{
                marginBottom: 0,
              }}
              title="Сохранить"
              onPress={() => saveAndBack()}
            />
          </View>
        </Card>
      </SafeAreaView>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  button: {
    width: "50%",
  },
  compositeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  header: {
    fontSize: 24,
    letterSpacing: 2,
    textTransform: "uppercase",
  },
  mainContainer: {
    padding: 15,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  subtitle: {
    fontSize: 16,
    letterSpacing: 1.8,
    textTransform: "uppercase",
  },
  text: {
    fontSize: 30,
  },
});
