import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import { Button, Icon } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";

import routes from "../navigation/routes";
import defaultStyle from "../config/styles";

export default function ActionWindow({ onEdit, onDelete }) {
  return (
    <>
      <TouchableOpacity>
        <Button
          title="Редактировать"
          icon={
            <Icon
              style={styles.icon}
              type="simple-line-icon"
              name="pencil"
              size={20}
            />
          }
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.titleStyle}
          onPress={() => onEdit()}
        />
      </TouchableOpacity>
      <TouchableOpacity>
        <Button
          title="Удалить"
          icon={<Icon style={styles.icon} type="font-awesome" name="trash-o" />}
          buttonStyle={styles.buttonStyle}
          titleStyle={styles.titleStyle}
          onPress={() => onDelete()}
        />
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  buttonStyle: {
    justifyContent: "flex-start",
    backgroundColor: defaultStyle.colors.white,
  },
  icon: {
    marginHorizontal: 10,
  },
  titleStyle: {
    color: defaultStyle.colors.black,
  },
  text: {
    fontSize: 22,
    marginBottom: 10,
    color: defaultStyle.colors.grey,
  },
});
