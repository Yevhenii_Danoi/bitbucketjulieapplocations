import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Modal } from "react-native";
import {
  GestureHandlerRootView,
  TouchableOpacity,
} from "react-native-gesture-handler";
import { Input, Icon } from "react-native-elements";
import * as Clipboard from "expo-clipboard";

import defaultStyle from "../config/styles";

export default function FirstLaunchModal({
  onClose,
  modalVisible,
  invitationLink,
}) {
  const [showTextCopiedNotification, setShowTextCopiedNotification] =
    useState("");

  const copyToClipboard = () => {
    Clipboard.setString(invitationLink);
    setShowTextCopiedNotification("Ссылка скопирована");
  };

  useEffect(() => {
    const timeoutHandle = setTimeout(() => {
      setShowTextCopiedNotification("");
    }, 2000);

    return () => {
      clearTimeout(timeoutHandle);
    };
  }, [showTextCopiedNotification]);

  return (
    <Modal
      visible={modalVisible && invitationLink.length != 0}
      animationType="slide"
    >
      <View style={styles.modalContainer}>
        <View style={{ flex: 1 }}>
          <Text style={styles.mainModalText}>Это наша изюминка!🍰</Text>
          <Text style={styles.basicModalText}>
            Пусть клиенты сами записываются к вам! Без регистрации!
          </Text>
          <Input
            rightIcon={
              <Icon
                type="material-community"
                name="content-copy"
                onPress={copyToClipboard}
              />
            }
            value={invitationLink}
            label="Онлайн-запись по ссылке"
            labelStyle={styles.labelStyle}
            inputContainerStyle={styles.inputContainerStyle}
            containerStyle={styles.containerStyle}
            textContentType="URL"
          />
          <Text style={styles.copiedText}>{showTextCopiedNotification}</Text>
          <Text style={styles.basicModalText}>
            Вы найдете ее в настройках профиля. Поместите ее там, где клиенты
            увидят😀
          </Text>
          <Text style={styles.basicModalText}>
            После первой онлайн-записи клиент получает свой уникальный
            20-значный код. Если он его потеряет, вы сможете переслать ему код
            повторно
          </Text>
        </View>
        <GestureHandlerRootView
          style={{ alignItems: "center", translateY: -40 }}
        >
          <TouchableOpacity onPress={onClose}>
            <Text style={styles.gotItText}>Понятно 🙂</Text>
          </TouchableOpacity>
        </GestureHandlerRootView>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  containerStyle: {
    paddingHorizontal: 0,
  },
  copiedText: {
    color: defaultStyle.colors.secondary,
  },
  gotItText: {
    fontSize: 20,
    letterSpacing: 0.5,
  },
  inputContainerStyle: {
    marginBottom: -20,
  },
  labelStyle: {
    marginBottom: 5,
  },
  modalContainer: {
    flex: 1,
    padding: 20,
  },
  mainModalText: {
    fontSize: 28,
    fontWeight: "700",
    marginBottom: 20,
  },
  basicModalText: {
    fontSize: 20,
    letterSpacing: 0.3,
    marginBottom: 20,
    lineHeight: 35,
  },
});
