import React, { useEffect, useState } from "react";
import {
  View,
  Alert,
  FlatList,
  StyleSheet,
  Text,
  Pressable,
} from "react-native";
import { SearchBar, ListItem, Avatar, Icon } from "react-native-elements";
import firebase from "firebase";

import { monthsNamesInDeclension } from "../config/calendar";
import { getCapitalLetters } from "../config/utils";
import defaultStyles from "../config/styles";
import ProjectActivityIndicator from "./ProjectActivityIndicator";
import { makeCall } from "../modules/utils/makeCall";
import BackgroundPicture from "./BackgroundPicture";

export default function ClientList({ onPress = (f) => f, route = {} }) {
  const [allClients, setClients] = useState([]);
  const [displayedClients, setDisplayedClients] = useState([]);

  const [isLoaded, setIsLoaded] = useState(false);
  const [hintText, setHintText] = useState(
    route.params
      ? "Добавьте своего первого клиента на вкладке Клиенты и возвращаетесь сюда"
      : "Добавьте своего первого клиента"
  );
  const [search, setSearch] = useState("");

  if (route.params) {
    onPress = route.params.onPress;
  }

  useEffect(() => {
    const unsubscribe = firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("clients")
      .orderBy("name", "asc")
      .onSnapshot(async (snapshot) => {
        const data = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        setClients(data);
        setDisplayedClients(data);
        setIsLoaded(true);
      });

    return unsubscribe;
  }, []);

  useEffect(() => {
    let clients = [];
    if (search.length > 0) {
      allClients.forEach((c) => {
        if (c.name.toLowerCase().includes(search.toLowerCase())) {
          clients.push(c);
        }
      });
      setDisplayedClients(clients);
    } else {
      setDisplayedClients(allClients);
    }
  }, [search]);

  const updateSearch = (search) => {
    setSearch(search);
  };

  if (isLoaded && allClients.length === 0)
    return (
      <BackgroundPicture source={require("../assets/images/clients.png")}>
        <Text style={styles.text}>
          Список клиентов пуст. {"\n"}
          {hintText}
        </Text>
      </BackgroundPicture>
    );

  if (isLoaded && allClients.length !== 0)
    return (
      <>
        <SearchBar
          placeholder="Введите здесь..."
          onChangeText={updateSearch}
          value={search}
        />
        {displayedClients.length === 0 && (
          <View style={styles.textContainer}>
            <Text style={styles.text}>Клиенты не найдены</Text>
          </View>
        )}
        <FlatList
          data={displayedClients}
          keyExtractor={(client) => client.id.toString()}
          renderItem={({ item }) => (
            <ListItem
              bottomDivider
              onPress={() => {
                onPress(item);
              }}
            >
              <Avatar
                containerStyle={{ backgroundColor: item.color }}
                title={getCapitalLetters(item.name)}
                titleStyle={{
                  fontSize: 18,
                  color: defaultStyles.colors.white,
                }}
                rounded
                size={50}
                // source={{ uri: item.avatar || "null" }}
              />
              <ListItem.Content>
                <ListItem.Title>{item.name}</ListItem.Title>
                <ListItem.Subtitle>
                  Последний раз был(а):
                  {item.lastVisit === "не был"
                    ? " не был"
                    : ` ${item.lastVisit.split("-")[2]} ${
                        monthsNamesInDeclension[
                          item.lastVisit.split("-")[1] - 1
                        ]
                      }`}
                </ListItem.Subtitle>
              </ListItem.Content>
              <Pressable
                onPress={() => makeCall(item.phoneNumber)}
                hitSlop={{ right: 50, top: 50, bottom: 50, left: 25 }}
              >
                <Icon
                  size={25}
                  type="font-awesome"
                  name="phone"
                  color={defaultStyles.colors.green}
                />
              </Pressable>
            </ListItem>
          )}
        />
      </>
    );

  return <ProjectActivityIndicator />;
}

const styles = StyleSheet.create({
  deleteButton: {
    minHeight: "100%",
    backgroundColor: "red",
  },
  text: {
    color: defaultStyles.colors.medium,
    fontSize: 16,
    textAlign: "center",
    lineHeight: 30,
    margin: 10,
    paddingHorizontal: 15,
  },
});
