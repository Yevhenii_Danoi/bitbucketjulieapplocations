import React from "react";
import { StyleSheet, Text } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Card } from "react-native-elements";

import defaultStyle from "../config/styles";

export default function NoDataContainer({ text }) {
  return (
    <SafeAreaView style={styles.container}>
      <Card containerStyle={styles.cardContainer}>
        <Text>{text}</Text>
      </Card>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  cardContainer: {
    margin: 0,
    borderRadius: 10,
    backgroundColor: defaultStyle.colors.light,
  },
  container: {
    flex: 1,
    paddingHorizontal: 15,
  },
});
