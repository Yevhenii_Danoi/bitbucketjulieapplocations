import React from "react";
import {
  Alert,
  Animated,
  Pressable,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { Card } from "react-native-elements";
import Swipable from "react-native-gesture-handler/Swipeable";
import firebase from "firebase";

import defaultStyle from "../../config/styles";

export default function FinancialNote({ data }) {
  const { id, name, category, price } = data;
  const deleteRecord = () => {
    try {
      firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .collection("finance")
        .doc(id)
        .delete();
    } catch (error) {
      Alert.alert(
        "Ошибка удаления",
        "По некоторым причинам не удалось удалить запись"
      );
    }
  };
  const renderRightActions = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
      extrapolate: "clamp",
    });
    return (
      <Pressable style={styles.rightActionContainer} onPress={deleteRecord}>
        <Animated.Text
          style={[styles.actionText, { transform: [{ translateX: scale }] }]}
        >
          Удалить
        </Animated.Text>
      </Pressable>
    );
  };
  return (
    <Swipable renderRightActions={renderRightActions}>
      <Card containerStyle={styles.cardContainer}>
        <View style={styles.cardAlignment}>
          <View style={styles.nameContainer}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.category}>{category}</Text>
          </View>
          <View style={styles.priceContainer}>
            <Text style={styles.priceText}>{price} грн.</Text>
          </View>
        </View>
      </Card>
    </Swipable>
  );
}

const styles = StyleSheet.create({
  actionText: {
    color: "#fff",
    fontWeight: "600",
    paddingHorizontal: 20,
  },
  addClientCircle: {
    position: "absolute",
    right: 0,
    bottom: 0,
    padding: 15,
  },

  cardContainer: {
    borderRadius: 5,
  },
  category: {
    fontSize: 16,
    letterSpacing: 0.9,
  },
  cardAlignment: {
    flexDirection: "row",
  },

  nameContainer: {
    flex: 1,
  },
  name: {
    fontWeight: "bold",
    fontSize: 18,
    letterSpacing: 0.7,
  },
  rightActionContainer: {
    backgroundColor: defaultStyle.colors.danger,
    justifyContent: "center",
    alignItems: "flex-end",
    marginTop: 15,
    marginRight: 5,
    marginLeft: -15,
    borderRadius: 5,
  },
  priceText: {
    fontSize: 20,
  },
  priceContainer: {
    justifyContent: "center",
  },
});
