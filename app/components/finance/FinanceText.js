import React from "react";
import { Pressable, StyleSheet, Text } from "react-native";

import defaultStyle from "../../config/styles";

export default function FinanceText({ text, style, onPress }) {
  return (
    <Pressable onPress={onPress}>
      <Text style={[styles.text, style]}>{text}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    textTransform: "uppercase",
    letterSpacing: 1.2,
    color: defaultStyle.colors.black,
  },
});
