import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Icon } from "react-native-elements";

export default function FinanceIcon({ type, name, text, isSelected, onPress }) {
  return (
    <View style={styles.container}>
      <Icon
        reverse
        onPress={onPress}
        type={type}
        name={name}
        color="#333"
        size={isSelected ? 40 : 30}
      />
      <Text style={styles.text}>{text}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginRight: 20,
  },
  text: {
    textAlign: "center",
  },
});
