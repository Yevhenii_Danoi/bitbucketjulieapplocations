import React, { useEffect, useState } from "react";
import { SectionList, StyleSheet, Text, View } from "react-native";
import { Icon, SpeedDial } from "react-native-elements";
import firebase from "firebase";
import { GestureHandlerRootView } from "react-native-gesture-handler";

import defaultStyle from "../../config/styles";
import FinanceIcon from "./FinanceIcon";
import FinanceText from "./FinanceText";
import FinancialNote from "./FinancialNote";
import routes from "../../navigation/routes";
import {
  todayCount,
  monthlyCount,
  yearCount,
} from "../../modules/utils/financeCount";
import createDataList from "../../modules/utils/createListToDisplayFinance";
import ProjectActivityIndicator from "../ProjectActivityIndicator";
import BackgroundPicture from "../BackgroundPicture";

export default function FinanceCard({ navigation }) {
  const [isLoaded, setIsLoaded] = useState(false);
  const [financeList, setFinanceList] = useState([]);
  const [dataList, setDataList] = useState([]);

  const [summa, setSumma] = useState(0);

  const [todayIsSelected, setTodayIsSelected] = useState(false);
  const [monthIsSelected, setMonthIsSelected] = useState(false);
  const [yearIsSelected, setYearIsSelected] = useState(false);

  const [incomeIsSelected, setIncomeIsSelected] = useState(false);
  const [netProfitIsSelected, setNetProfitIsSelected] = useState(false);
  const [costsIsSelected, setCostsIsSelected] = useState(false);

  const [openDial, setOpenDial] = useState(false);

  useEffect(() => {
    const subscribe = firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("finance")
      .onSnapshot((snapshot) => {
        const data = snapshot.docs.map((doc) => ({
          id: doc.id,
          ...doc.data(),
        }));
        data.sort((a, b) => (a.date < b.date ? 1 : -1));
        setFinanceList(data);
      });
    setTodayIsSelected(true);
    setIncomeIsSelected(true);

    return () => subscribe();
  }, []);

  useEffect(() => {
    calculateIncome();
  }, [financeList]);

  useEffect(() => {
    setDataList(createDataList(financeList));
    setIsLoaded(true);
  }, [financeList]);

  useEffect(() => {
    if (incomeIsSelected) calculateIncome();
    else if (netProfitIsSelected) calculateNetProfit();
    else if (costsIsSelected) calculateCosts();
  }, [todayIsSelected, monthIsSelected, yearIsSelected]);

  const calculateIncome = () => {
    if (todayIsSelected) setSumma(todayCount(financeList, "income"));
    else if (monthIsSelected) setSumma(monthlyCount(financeList, "income"));
    else if (yearIsSelected) setSumma(yearCount(financeList, "income"));
  };

  const calculateNetProfit = () => {
    if (todayIsSelected) setSumma(todayCount(financeList, "netProfit"));
    else if (monthIsSelected) setSumma(monthlyCount(financeList, "netProfit"));
    else if (yearIsSelected) setSumma(yearCount(financeList, "netProfit"));
  };

  const calculateCosts = () => {
    if (todayIsSelected) setSumma(todayCount(financeList, "costs"));
    else if (monthIsSelected) setSumma(monthlyCount(financeList, "costs"));
    else if (yearIsSelected) setSumma(yearCount(financeList, "costs"));
  };

  return (
    <>
      <View style={styles.invoiceContainer}>
        <View style={styles.headerOptionContainer}>
          <FinanceText
            text="Сегодня"
            onPress={() => {
              setTodayIsSelected(true);
              setMonthIsSelected(false);
              setYearIsSelected(false);
            }}
            style={todayIsSelected && { color: defaultStyle.colors.secondary }}
          />
          <FinanceText
            onPress={() => {
              setTodayIsSelected(false);
              setMonthIsSelected(true);
              setYearIsSelected(false);
            }}
            text="Месяц"
            style={[
              { marginHorizontal: 25 },
              monthIsSelected && { color: defaultStyle.colors.secondary },
            ]}
          />
          <FinanceText
            onPress={() => {
              setTodayIsSelected(false);
              setMonthIsSelected(false);
              setYearIsSelected(true);
            }}
            text="Год"
            style={yearIsSelected && { color: defaultStyle.colors.secondary }}
          />
        </View>
        <Text style={styles.account}>{summa} USD</Text>
        <View style={styles.footerOptionContainer}>
          <FinanceIcon
            isSelected={incomeIsSelected}
            type="font-awesome"
            name="line-chart"
            text="Доход"
            onPress={() => {
              setIncomeIsSelected(true);
              setCostsIsSelected(false);
              setNetProfitIsSelected(false);
              calculateIncome();
            }}
          />
          <FinanceIcon
            isSelected={netProfitIsSelected}
            type="ionicon"
            name="stats-chart-sharp"
            text="Чистая"
            onPress={() => {
              setIncomeIsSelected(false);
              setCostsIsSelected(false);
              setNetProfitIsSelected(true);
              calculateNetProfit();
            }}
          />
          <FinanceIcon
            onPress={() => {
              setIncomeIsSelected(false);
              setCostsIsSelected(true);
              setNetProfitIsSelected(false);
              calculateCosts();
            }}
            isSelected={costsIsSelected}
            type="antdesign"
            name="piechart"
            text="Расход"
          />
        </View>
      </View>
      <GestureHandlerRootView style={{ flex: 1 }}>
        <View style={styles.financialNotesContainer}>
          {isLoaded && dataList.length > 0 ? (
            <SectionList
              renderItem={({ item }) => <FinancialNote data={item} />}
              renderSectionHeader={({ section: { title } }) => (
                <View style={styles.titleContainer}>
                  <Text style={styles.title}>{title}</Text>
                </View>
              )}
              sections={dataList}
              keyExtractor={(item) => item.id.toString()}
            />
          ) : !isLoaded ? (
            <ProjectActivityIndicator />
          ) : isLoaded && !dataList.length > 0 ? (
            <BackgroundPicture
              source={require("../../assets/images/money.png")}
            />
          ) : null}

          <SpeedDial
            isOpen={openDial}
            color="transparent"
            icon={<Icon type="antdesign" name="plus" color="#fff" />}
            openIcon={<Icon type="antdesign" name="close" color="#fff" />}
            onOpen={() => setOpenDial(!openDial)}
            onClose={() => setOpenDial(!openDial)}
            overlayColor="rgba(0,0,0,.1)"
            containerStyle={{
              backgroundColor: "#333",
            }}
          >
            <SpeedDial.Action
              color="transparent"
              icon={<Icon type="font-awesome" name="line-chart" size={20} />}
              containerStyle={{
                backgroundColor: "#fff",
              }}
              title="Доход"
              onPress={() => {
                navigation.navigate(routes.FINANCE_CREATE_RECORD, {
                  operation: "income",
                });
                setOpenDial(!openDial);
              }}
            />
            <SpeedDial.Action
              color="transparent"
              icon={<Icon type="antdesign" name="piechart" size={20} />}
              containerStyle={{
                backgroundColor: "#fff",
              }}
              title="Расход"
              onPress={() => {
                navigation.navigate(routes.FINANCE_CREATE_RECORD, {
                  operation: "expense",
                });
                setOpenDial(!openDial);
              }}
            />
          </SpeedDial>
        </View>
      </GestureHandlerRootView>
    </>
  );
}

const styles = StyleSheet.create({
  account: {
    fontSize: 32,
    letterSpacing: 1,
    marginVertical: 25,
  },
  financialNotesContainer: {
    flex: 1,
    paddingVertical: 15,
  },
  footerOptionContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "baseline",
  },
  headerOptionContainer: {
    flexDirection: "row",
  },
  invoiceContainer: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: defaultStyle.colors.light,
  },
  title: {
    fontWeight: "bold",
    fontSize: 16,
  },
  titleContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 15,
    marginTop: 15,
  },
});
