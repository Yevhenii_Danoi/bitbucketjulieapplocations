import React from "react";
import { Pressable, StyleSheet, Text, View } from "react-native";

import defaultStyle from "../config/styles";
import ExecutionLine from "./ExecutionLine";
import useAuth from "../auth/useAuth";

export default function RegisterExecutionLines({ step = 1 }) {
  const { logout } = useAuth();

  return (
    <>
      <Pressable style={styles.logoutContainer} onPress={() => logout()}>
        <Text style={styles.logoutText}>Выйти</Text>
      </Pressable>
      <View style={styles.container}>
        <ExecutionLine color={defaultStyle.colors.secondary} />
        <View>
          <Text style={styles.text}></Text>
        </View>
        <ExecutionLine
          color={
            step > 1 ? defaultStyle.colors.secondary : defaultStyle.colors.white
          }
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
  logoutText: {
    color: defaultStyle.colors.secondary,
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 3,
    textTransform: "uppercase",
  },
  logoutContainer: {
    alignItems: "flex-end",
  },
  text: {
    width: 25,
  },
});
