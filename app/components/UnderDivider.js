import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import { Button, Divider, Icon, ThemeProvider } from "react-native-elements";
import { FormikContext } from "formik";
import {
  GestureHandlerRootView,
  TouchableOpacity,
} from "react-native-gesture-handler";

import defaultStyle from "../config/styles";

export default function UnderDivider({
  disabled = false,
  icon,
  buttonTitle,
  onPress,
  style,
  authScreen = false,
  onLayout = null,
}) {
  const formikContext = useContext(FormikContext);

  if (!onPress && formikContext) onPress = formikContext.handleSubmit;

  return (
    <GestureHandlerRootView>
      <View style={[styles.container, style]} onLayout={onLayout}>
        <Divider
          orientation="horizontal"
          width={3}
          style={styles.dividerStyle}
        />
        <View style={styles.contentContainer}>
          <ThemeProvider theme={defaultStyle.theme}>
            <TouchableOpacity activeOpacity={1}>
              <Button
                disabled={disabled}
                title={buttonTitle}
                icon={
                  icon && (
                    <Icon
                      type="material-community"
                      name={icon}
                      style={{ marginRight: 20 }}
                      color="#fff"
                    />
                  )
                }
                iconRight
                onPress={onPress}
              />
            </TouchableOpacity>
          </ThemeProvider>
        </View>
      </View>
    </GestureHandlerRootView>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "flex-end",
  },
  contentContainer: {
    paddingHorizontal: 15,
  },
  dividerStyle: {
    marginBottom: 15,
  },
  tinyLogo: {
    width: 35,
    height: 35,
    marginRight: 20,
  },
});
