import React, { useState, useEffect } from "react";
import {
  Pressable,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Platform,
  Alert,
} from "react-native";
import DateTimePickerResult from "@react-native-community/datetimepicker";
import { Divider, ListItem, Icon } from "react-native-elements";
import defaultStyle from "../config/styles";
import dayjs from "dayjs";
import firebase from "firebase";
import OverlayAI from "../components/OverlayAI";

import { monthsNamesInDeclension } from "../config/calendar";
import routes from "../navigation/routes";
import UnderDivider from "./UnderDivider";
import dateCercion from "../modules/utils/getFreeWorkTime";

export default function CreateRecord({ navigation, route }) {
  const { client } = route.params;

  const [expandedProcedures, setExpandedProcedures] = useState(false);
  const [expandedTime, setExpandedTime] = useState(false);

  const [isLoaded, setIsLoaded] = useState(false);
  const [submitting, setSubmitting] = useState(false);

  const [isCounting, setIsCounting] = useState(undefined);
  const [isCounted, setIsCounted] = useState(undefined);
  const [isDisable, setIsDisable] = useState(true);
  const [isTrackedChanges, setIsTrackedChanges] = useState(false);
  const [startTimeDataTracking, setStartTimeDataTracking] = useState(null);

  const [areas, setAreas] = useState([]);
  const [freeWorkTimes, setFreeWorkTimes] = useState([
    "Выберите услугу для расчета времени и дату",
  ]);
  const [isDisabledItems, setIsDisabledItems] = useState(true);
  const [procedureData, setProcedureData] = useState([]);

  const [displayProcedure, setDisplayProcedure] = useState("Выберите услугу");
  const [displayDate, setDisplayDate] = useState("Выберите дату");
  const [displayTime, setDisplayTime] = useState("Выберите доступное время");

  const [showDate, setShowDate] = useState(false);

  const [date, setDate] = useState(new Date());
  const [isDateSelected, setIsDateSelected] = useState(false);
  const [isDateDisabled, setIsDateDisabled] = useState(true);

  const [startOfDayTime, setStartOfDayTime] = useState(undefined);
  const [endOfDayTime, setEndOfDayTime] = useState(undefined);
  const [hasWorkTimeSet, setHasWorkTimeSet] = useState(false);
  const [startLunchTime, setStartLunchTime] = useState(undefined);
  const [endLunchTime, setEndLunchTime] = useState(undefined);
  const [hasLunchTimeSet, setHasLunchTimeSet] = useState(false);

  const [allTimesBetweenEntries, setAllTimesBetweenEntries] =
    useState(undefined);
  const [possibleTimesBetweenEntries, setPossibleTimesBetweenEntries] =
    useState(undefined);

  const [smallestTimeInterval, setSmallestTimeInterval] = useState(undefined);

  const [listScrollHeight, setListScrollHeight] = useState(null);
  const [isHeightSet, setIsHeightSet] = useState(false);

  const [wholeScreenHeight, setWholeScreenHeight] = useState(null);
  const [underDividerHeight, setUnderDividerHeight] = useState(null);
  const [mainComponentsHeight, setMainComponentsHeight] = useState(null);

  useEffect(() => {
    if (wholeScreenHeight && underDividerHeight && mainComponentsHeight) {
      setListScrollHeight(
        wholeScreenHeight - (underDividerHeight + mainComponentsHeight)
      );
      setIsHeightSet(true);
    }
  }, [wholeScreenHeight, underDividerHeight, mainComponentsHeight]);

  const docRef = firebase
    .firestore()
    .collection("users")
    .doc(firebase.auth().currentUser.uid)
    .collection("areas");

  useEffect(() => {
    const fetchAreasData = async () => {
      const areasSnapshot = await docRef.get();

      const getPriceList = async (area) => {
        let priceListData = [];
        const dbPriceList = await docRef
          .doc(area.id)
          .collection("price-list")
          .get();

        dbPriceList.docs.forEach((item) => {
          priceListData.push({
            areaID: area.id,
            procedureID: item.id,
            area: area.data().area,
            procedure: item.data().procedure,
            duration: item.data().hours * 60 + item.data().minutes,
          });
        });
        return priceListData;
      };

      let areasData = [];
      await Promise.all(
        areasSnapshot.docs.map(async (area) => {
          const priceList = await getPriceList(area);
          areasData.push(priceList);
        })
      ).then(() => {
        setAreas(areasData.flat());
        setIsLoaded(true);
      });
    };
    fetchAreasData();
  }, []);

  useEffect(() => {
    if (areas.length > 0) {
      const sortAreas = areas.sort((a, b) => a.duration > b.duration);
      setSmallestTimeInterval(sortAreas[0].duration);
    }
  }, [areas]);

  useEffect(() => {
    if (isCounted) {
      setIsDateDisabled(false);
    }
    if (
      freeWorkTimes.length > 0 &&
      isCounted &&
      freeWorkTimes[0] !== "Нет свободных мест. Вы молодцы!" &&
      freeWorkTimes[0] !== "В этот день у вас выходной" &&
      !isTrackedChanges
    ) {
      setStartTimeDataTracking(new Date());
      setIsTrackedChanges(true);
    } else if (freeWorkTimes.length === 0) {
      setFreeWorkTimes(["Нет свободных мест. Вы молодцы!"]);
      setIsDisabledItems(true);
    }
  }, [isCounted]);

  useEffect(() => {
    let isMounted = true;

    if (isTrackedChanges) {
      firebase
        .firestore()
        .collection("records")
        .where("date", "==", dayjs(date).toISOString().split("T", 1)[0])
        .onSnapshot((snapshot) => {
          snapshot.docChanges().forEach((change) => {
            if (change.type === "added") {
              if (
                change.doc.data().lastModified.seconds * 1000 >=
                  startTimeDataTracking.getTime() &&
                isMounted
              ) {
                if (!submitting) {
                  setDisplayTime("Выберите доступное время");
                  setIsDateSelected(true);
                }
              }
            }
          });
        });

      firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .collection("records")
        .where("date", "==", dayjs(date).toISOString().split("T", 1)[0])
        .onSnapshot((snapshot) => {
          snapshot.docChanges().forEach((change) => {
            if (change.type !== "modified") {
              if (
                (change.type === "removed" ||
                  change.doc.data().lastModified.seconds * 1000 >=
                    startTimeDataTracking.getTime()) &&
                isMounted
              ) {
                if (!submitting) {
                  setDisplayTime("Выберите доступное время");
                  setIsDateSelected(true);
                }
              }
            }
          });
        });
    }
    return () => {
      isMounted = false;
    };
  }, [isTrackedChanges, date, submitting]);

  useEffect(() => {
    if (isCounting) {
      setIsCounted(false);
      setFreeWorkTimes(["Минутку. Идёт расчет..."]);
      setIsDisabledItems(true);
      setIsDateDisabled(true);
    } else if (isCounting === false) {
      setIsCounted(true);
    }
  }, [isCounting]);

  useEffect(() => {
    if (procedureData[2]) {
      if (displayDate === "Выберите дату") setFreeWorkTimes(["Выберите дату"]);
      setIsDateDisabled(false);
      onDateChange({ type: "set" }, date);
    }
  }, [procedureData]);

  useEffect(() => {
    if (
      displayProcedure !== "Выберите услугу" &&
      displayDate !== "Выберите дату" &&
      displayTime !== "Выберите доступное время"
    ) {
      setIsDisable(false);
    } else {
      setIsDisable(true);
    }
  }, [displayDate, displayProcedure, displayTime]);

  useEffect(() => {
    const getStartEndTimeOfWorkDay = async () => {
      if (isDateSelected && date) {
        setIsCounting(true);
        await firebase
          .firestore()
          .collection("users")
          .doc(firebase.auth().currentUser.uid)
          .collection("schedule")
          .where("dayID", "==", new Date(date).getDay())
          .get()
          .then(async (dayData) => {
            let start = await dayData.docs[0].data().startWorkTime;

            let end = await dayData.docs[0].data().endWorkTime;

            setStartOfDayTime(start ? dateCercion(start.toDate(), date) : null);
            setEndOfDayTime(end ? dateCercion(end.toDate(), date) : null);
          })
          .then(() => {
            setHasWorkTimeSet(true);
          });
      }
    };

    getStartEndTimeOfWorkDay().then(() => {
      if (isDateSelected) setIsDateSelected(false);
    });
  }, [isDateSelected]);

  useEffect(() => {
    let day = new Date(date).getDay();
    if (hasWorkTimeSet) {
      if (startOfDayTime && endOfDayTime) {
        firebase
          .firestore()
          .collection("users")
          .doc(firebase.auth().currentUser.uid)
          .collection("schedule")
          .where("dayID", "==", day)
          .get()
          .then(async (dayData) => {
            let start = await dayData.docs[0].data().startLunchTime;
            let end = await dayData.docs[0].data().endLunchTime;

            setStartLunchTime(start ? dateCercion(start.toDate(), date) : null);
            setEndLunchTime(end ? dateCercion(end.toDate(), date) : null);
            setHasLunchTimeSet(true);
          })
          .then(() => setHasWorkTimeSet(false));
      } else {
        setFreeWorkTimes(["В этот день у вас выходной"]);
        setIsDisabledItems(true);
        if (hasWorkTimeSet) setHasWorkTimeSet(false);
        setIsCounting(false);
      }
    }
  }, [hasWorkTimeSet]);
  let getRegularRecords = () => {
    let proceduresTime = [];
    return new Promise(async (resolve) => {
      const dbRecords = firebase
        .firestore()
        .collection("users")
        .doc(firebase.auth().currentUser.uid)
        .collection("records")
        .where("date", "==", dayjs(date).toISOString().split("T", 1)[0])
        .get();

      const recSnapshot = await dbRecords;

      if (recSnapshot.docs.length > 0)
        recSnapshot.docs.forEach((rec) => {
          proceduresTime.push({
            start: dateCercion(rec.data().startTime.toDate(), date),
            end: dateCercion(rec.data().endTime.toDate(), date),
          });
        });
      else proceduresTime = [];

      resolve(proceduresTime);
    });
  };

  let getOnlineRecords = () => {
    let proceduresTime = [];
    return new Promise(async (resolve) => {
      const dbRecords = firebase
        .firestore()
        .collection("records")
        .where("date", "==", dayjs(date).toISOString().split("T", 1)[0])
        .get();

      const recSnapshot = await dbRecords;
      if (recSnapshot.docs.length > 0)
        recSnapshot.docs.forEach((rec) => {
          proceduresTime.push({
            start: dateCercion(rec.data().startTime.toDate(), date),
            end: dateCercion(rec.data().endTime.toDate(), date),
          });
        });
      else proceduresTime = [];
      resolve(proceduresTime);
    });
  };
  useEffect(() => {
    const getRecordOnThisDate = async () => {
      if (hasLunchTimeSet) {
        startCalculatingFreeTime();
      }
    };

    getRecordOnThisDate();
  }, [hasLunchTimeSet]);

  const startCalculatingFreeTime = async () => {
    let regularRecords = getRegularRecords();
    let onlineRecords = getOnlineRecords();

    await Promise.all([regularRecords, onlineRecords])
      .then((one) => {
        if (startLunchTime && endLunchTime) {
          if (
            one[0]
              .concat(one[1])
              .concat({ start: startLunchTime, end: endLunchTime }).length > 0
          ) {
            calculateTimeBetweenRecords(
              one[0]
                .concat(one[1])
                .concat({ start: startLunchTime, end: endLunchTime })
            );
          } else {
            setAllTimesBetweenEntries([]);
          }
        } else {
          if (one[0].concat(one[1]).length > 0) {
            calculateTimeBetweenRecords(one[0].concat(one[1]));
          } else {
            setAllTimesBetweenEntries([]);
          }
        }
      })
      .then(() => {
        if (hasLunchTimeSet) setHasLunchTimeSet(false);
      });
  };

  const createRecordInDatabase = async () => {
    let startTime = displayTime.split(":");

    let start = date;
    let end = date;

    start.setHours(+startTime[0]);
    start.setMinutes(+startTime[1]);
    start.setSeconds(0);

    end = new Date(new Date(date).setMinutes(+startTime[1] + procedureData[2]));

    setSubmitting(true);

    await firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("records")
      .add({
        areaID: procedureData[0],
        clientID: client.id,
        date: new Date(date.getTime() - date.getTimezoneOffset() * 60000)
          .toISOString()
          .split("T")[0],
        startTime: start,
        endTime: end,
        isDone: false,
        procedureID: procedureData[1],
        lastModified: new Date(),
      })
      .then(() => {
        setSubmitting(false);
        navigation.navigate(routes.PLANNER);
      });
  };

  useEffect(() => {
    if (allTimesBetweenEntries) {
      let possibleTimes = [];
      if (allTimesBetweenEntries.length > 0) {
        allTimesBetweenEntries.forEach((t) => {
          if (new Date(t.start.getTime() + procedureData[2] * 60000) <= t.end)
            possibleTimes.push(t);
        });
      } else {
        possibleTimes.push({
          start: startOfDayTime,
          end: endOfDayTime,
        });
      }
      setPossibleTimesBetweenEntries(possibleTimes);
    }
  }, [allTimesBetweenEntries]);

  useEffect(() => {
    if (possibleTimesBetweenEntries) {
      // if the array contains one object with timestamps corresponding to the beginning and end of the working day
      if (possibleTimesBetweenEntries.length > 0) {
        let times = [];
        if (
          possibleTimesBetweenEntries.length === 1 &&
          startOfDayTime.toString().normalize() ===
            possibleTimesBetweenEntries[0].start.toString().normalize() &&
          endOfDayTime.toString().normalize() ===
            possibleTimesBetweenEntries[0].end.toString().normalize()
        ) {
          let startTime = startOfDayTime;
          let endTime = endOfDayTime;
          endTime.setMinutes(endOfDayTime.getMinutes() - procedureData[2]);
          while (startTime <= endTime) {
            let minutes = "0" + startTime.getMinutes();
            times.push(`${startTime.getHours()}:${minutes.slice(-2)}`);
            startTime.setMinutes(startTime.getMinutes() + smallestTimeInterval);
          }
        } else {
          possibleTimesBetweenEntries.forEach((t) => {
            let startTime = t.start;
            let endTime = t.end;
            endTime.setMinutes(endTime.getMinutes() - procedureData[2]);
            while (startTime <= endTime) {
              let minutes = "0" + t.start.getMinutes();
              times.push(`${t.start.getHours()}:${minutes.slice(-2)}`);
              startTime.setMinutes(
                startTime.getMinutes() + smallestTimeInterval
              );
            }
          });
        }
        setFreeWorkTimes(times);
        setIsDisabledItems(false);
      } else {
        setFreeWorkTimes([`Нет свободных мест для '${displayProcedure}'`]);
        setIsDisabledItems(true);
      }
      setIsCounting(false);
    }
  }, [possibleTimesBetweenEntries]);

  const calculateTimeBetweenRecords = async (proceduresTimes) => {
    let betweenEntries = [];
    let start = undefined;
    let i = 0;
    proceduresTimes.sort((a, b) => a.start - b.start);
    //Определение начала времени на этот день
    if (
      startOfDayTime.toString().normalize() !==
      proceduresTimes[0].start.toString().normalize()
    ) {
      start = startOfDayTime;
      i = -1;
    } else {
      start = proceduresTimes[0].end;
    }
    //Рассчитать промежутки
    for (let j = i; j < proceduresTimes.length; j++) {
      if (proceduresTimes[j + 1] !== undefined) {
        if (start < proceduresTimes[j + 1].start) {
          betweenEntries.push({
            start: start,
            end: proceduresTimes[j + 1].start,
          });
        }
        start = proceduresTimes[j + 1].end;
      }
    }
    if (start < endOfDayTime)
      betweenEntries.push({
        start: start,
        end: endOfDayTime,
      });
    setAllTimesBetweenEntries(betweenEntries);
  };

  const onDateChange = async (event, selectedDate) => {
    setShowDate(Platform.OS === "ios");
    if (selectedDate) {
      setDate(selectedDate);
      setIsDateSelected(true);
    }
    if (event && event.type === "set") {
      const currentDate = dayjs(selectedDate).toISOString().split("T", 1)[0];
      setDisplayDate(
        `${currentDate.split("-")[2]} ${
          monthsNamesInDeclension[currentDate.split("-")[1] - 1]
        }, ${currentDate.split("-")[0]}`
      );
    }
  };

  const showDatepicker = () => {
    if (isDateDisabled)
      Alert.alert(
        "Пожалуйста,  выберите услугу",
        "Чтобы выбрать дату, пожалуйста, выберите услугу"
      );
    else setShowDate(true);
  };

  if (!isLoaded) return <OverlayAI visible={true} />;

  return (
    <>
      <OverlayAI visible={submitting} />
      <View
        style={{
          flex: 1,
          justifyContent: "space-between",
          backgroundColor: defaultStyle.colors.light,
        }}
        onLayout={(e) => {
          if (!isHeightSet) setWholeScreenHeight(e.nativeEvent.layout.height);
        }}
      >
        <View
          onLayout={(e) => {
            if (!isHeightSet)
              setMainComponentsHeight(e.nativeEvent.layout.height);
          }}
        >
          <ListItem.Accordion
            content={
              <ListItem.Content>
                <ListItem.Title style={styles.text}>Вид услуги</ListItem.Title>
                <ListItem.Subtitle>{displayProcedure}</ListItem.Subtitle>
              </ListItem.Content>
            }
            isExpanded={expandedProcedures}
            onPress={() => {
              setExpandedProcedures(!expandedProcedures);
            }}
            containerStyle={[
              styles.listItemContainer,
              styles.listItemAccordionContainer,
            ]}
          >
            <Divider width={3} />
            <ScrollView style={{ height: listScrollHeight }}>
              {expandedProcedures &&
                areas
                  .sort(
                    (a, b) =>
                      a.area + " | " + a.procedure >
                      b.area + " | " + b.procedure
                  )
                  .map((item) => (
                    <ListItem
                      key={item.procedureID.toString()}
                      bottomDivider
                      onPress={() => {
                        setDisplayProcedure(`${item.area} | ${item.procedure}`);
                        setProcedureData([
                          item.areaID,
                          item.procedureID,
                          item.duration,
                        ]);
                        setExpandedProcedures(!expandedProcedures);
                      }}
                      containerStyle={[styles.listItemContainer]}
                    >
                      <ListItem.Content>
                        <ListItem.Title>
                          {item.area} | {item.procedure}
                        </ListItem.Title>
                      </ListItem.Content>
                    </ListItem>
                  ))}
            </ScrollView>
          </ListItem.Accordion>
          <Divider width={2} color={defaultStyle.colors.light} />

          <Pressable onPress={showDatepicker}>
            <View style={styles.listItemContainer}>
              <Text style={styles.text}>Дата</Text>
              <Text>{displayDate}</Text>
            </View>
          </Pressable>

          <Divider width={2} color={defaultStyle.colors.light} />

          <ListItem.Accordion
            content={
              <>
                <ListItem.Content>
                  <ListItem.Title style={styles.text}>Время</ListItem.Title>
                  <ListItem.Subtitle>{displayTime}</ListItem.Subtitle>
                </ListItem.Content>
              </>
            }
            isExpanded={expandedTime}
            onPress={() => {
              setExpandedTime(!expandedTime);
            }}
            containerStyle={[
              styles.listItemContainer,
              styles.listItemAccordionContainer,
            ]}
          >
            <Divider width={3} />
            <ScrollView style={{ height: listScrollHeight }}>
              {expandedTime &&
                freeWorkTimes.map((item) => (
                  <ListItem
                    disabled={isDisabledItems}
                    key={item}
                    bottomDivider
                    containerStyle={styles.listItemContainer}
                    onPress={() => {
                      setDisplayTime(`${item}`);
                      setExpandedTime(!expandedTime);
                    }}
                  >
                    <ListItem.Content>
                      <ListItem.Title
                        style={
                          isDisabledItems && {
                            color: defaultStyle.colors.gray,
                          }
                        }
                      >
                        {item}
                      </ListItem.Title>
                    </ListItem.Content>
                  </ListItem>
                ))}
            </ScrollView>
          </ListItem.Accordion>
        </View>

        <UnderDivider
          onLayout={(e) => {
            if (!isHeightSet)
              setUnderDividerHeight(e.nativeEvent.layout.height);
          }}
          buttonTitle="Готово"
          disabled={isDisable}
          onPress={createRecordInDatabase}
        />
        {showDate && (
          <DateTimePickerResult
            testID="datePicker"
            value={date}
            mode="datetime"
            display="default"
            onChange={onDateChange}
            minimumDate={new Date()}
          />
        )}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    flex: 1,
    flexGrow: 1,
    backgroundColor: defaultStyle.colors.light,
  },
  listItemContainer: {
    padding: 25,
    backgroundColor: defaultStyle.colors.white,
  },
  listItemAccordionContainer: {
    alignItems: "flex-start",
  },
  icon: { marginRight: 5 },
  text: {
    fontSize: 18,
    letterSpacing: 0.6,
  },
  underDividerContainer: {
    paddingHorizontal: 10,
  },
});
