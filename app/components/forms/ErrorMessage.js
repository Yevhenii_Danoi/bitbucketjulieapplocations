import React from "react";
import { StyleSheet, Text, View } from "react-native";

import defaultStyle from "../../config/styles";

export default function ErrorMessage({ error, visible }) {
  if (!error || !visible) return null;

  return (
    <View>
      <Text style={styles.error}>{error}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  error: {
    color: defaultStyle.colors.danger,
    marginTop: -20,
    marginBottom: 20,
  },
});
