import React, { useEffect, useState } from "react";
import { StyleSheet, Text } from "react-native";
import TextInput from "../TextInput";
import ErrorMessage from "./ErrorMessage";
import { useFormikContext } from "formik";

import defaultStyle from "../../config/styles";

export default function FormField({ name, ...otherProps }) {
  const { setFieldTouched, handleChange, errors, touched } = useFormikContext();
  const [passwordRestrictionText, setPasswordRestrictionText] = useState(
    "8-30 символов, Одна заглавная, Одна прописная, Одна цифра"
  );

  useEffect(() => {
    if (!errors[name] || !touched[name])
      setPasswordRestrictionText(
        "8-30 символов, Одна заглавная, Одна прописная, Одна цифра"
      );
    else setPasswordRestrictionText("");
  }, [errors[name], touched[name]]);
  return (
    <>
      <TextInput
        onBlur={() => setFieldTouched(name)}
        onChangeText={handleChange(name)}
        {...otherProps}
      />
      {name === "newPassword" && (!errors[name] || !touched[name]) ? (
        <Text style={styles.text}>{passwordRestrictionText}</Text>
      ) : (
        <ErrorMessage error={errors[name]} visible={touched[name]} />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  text: {
    color: defaultStyle.colors.medium,
    marginTop: -20,
    marginBottom: 20,
  },
});
