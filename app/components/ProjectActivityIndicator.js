import React from "react";
import { StyleSheet, ActivityIndicator, View } from "react-native";

import defaultStyles from "../config/styles";

export default function ProjectActivityIndicator({ style, size = 50 }) {
  return (
    <View style={[styles.activityIndicatorContainer, { ...style }]}>
      <ActivityIndicator size={size} color={defaultStyles.colors.black} />
    </View>
  );
}

const styles = StyleSheet.create({
  activityIndicatorContainer: {
    flex: 1,
    justifyContent: "center",
  },
});
