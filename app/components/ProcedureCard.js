import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card } from "react-native-elements";
import ProjectActivityIndicator from "../components/ProjectActivityIndicator";

import { monthsNamesInDeclension } from "../config/calendar";
import firebase from "firebase";

export default function ProcedureCard({ record }) {
  const { date, procedureID, areaID } = record;

  const [dateOutput, setDateOutput] = useState("");
  const [price, setPrice] = useState(undefined);
  const [procedure, setProcedure] = useState(undefined);

  const [isLoaded, setIsLoaded] = useState(false);

  const recordDate = new Date(date);
  const weekday = new Array("вс", "пн", "вт", "ср", "чт", "пт", "сб");
  const dateComponents = date.split("-");

  useEffect(() => {
    getPriceAndName();
    let monthNumber = parseInt(dateComponents[1]) - 1;
    const monthName = monthsNamesInDeclension[monthNumber];
    const dayName = weekday[recordDate.getDay()];
    const year = recordDate.getFullYear();

    const dateFormatting = `${dayName}, ${dateComponents[2]} ${monthName} ${year}`;
    setDateOutput(dateFormatting);
  }, []);

  const getPriceAndName = () => {
    firebase
      .firestore()
      .collection("users")
      .doc(firebase.auth().currentUser.uid)
      .collection("areas")
      .doc(areaID)
      .collection("price-list")
      .doc(procedureID)
      .get()
      .then((snapshot) => {
        setPrice(snapshot.data().price);
        setProcedure(snapshot.data().procedure);
        setIsLoaded(true);
      });
  };

  if (isLoaded)
    return (
      <View style={styles.container}>
        <Card>
          <Card.Title style={styles.text}>{procedure}</Card.Title>
          <View style={styles.subtitleContainer}>
            <Text>{dateOutput}</Text>
            <Text>{price} грн.</Text>
          </View>
        </Card>
      </View>
    );

  return <ProjectActivityIndicator />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subtitleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  text: {
    textAlign: "left",
  },
});
