import React, { useEffect, useState, useRef } from "react";
import { AppState, LogBox } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import firebase from "firebase";
import * as Notifications from "expo-notifications";
import * as Sentry from "sentry-expo";

import * as AppConstants from "./app/config/constants";
import AuthNavigator from "./app/navigation/AuthNavigator";
import navigationTheme from "./app/navigation/navigationTheme";
import AppNavigator from "./app/navigation/AppNavigator";
import settings from "./app/config/settings";
import { AuthProvider } from "./app/auth/context";
import BasicAccountSettings from "./app/navigation/BasicAccountSettings";
import { NetworkProvider } from "react-native-offline";
import NetInfo from "@react-native-community/netinfo";
import OfflineScreen from "./app/screens/OfflineScreen";
import AddingAvatar from "./app/screens/InitialAccountSetup/AddingAvatar";
import OfflineNotice from "./app/components/OfflineNotice";

if (firebase.apps.length === 0) {
  firebase.initializeApp(settings.firebaseConfig);
}

Sentry.init({
  dsn: "https://be43a028008e4750aa42dbe6d95eaa0e@o1101496.ingest.sentry.io/6127573",
  enableInExpoDevelopment: true,
  debug: true, // If `true`, Sentry will try to print out useful debugging information if something goes wrong with sending the event. Set it to `false` in production
});

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: false,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App() {
  const [currentUser, setCurrentUser] = useState();
  const [loading, setLoading] = useState(true);
  const [loadingSettings, setLoadingSettings] = useState(true);
  const [isCompletedConfiguration, setIsCompletedConfiguration] =
    useState(false);
  const [currentUserData, setCurrentUserData] = useState({});

  const [isConnected, setIsConnected] = useState(false);
  const [notification, setNotification] = useState(undefined);

  const appState = useRef(AppState.currentState);

  useEffect(() => {
    LogBox.ignoreLogs(["Setting a timer"]);
    LogBox.ignoreLogs([
      "Non-serializable values were found in the navigation state",
    ]);
    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      setCurrentUser(user);
      setLoading(false);
    });

    return unsubscribe;
  }, []);

  useEffect(() => {
    const subscription = AppState.addEventListener("change", (nextAppState) => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === "active"
      ) {
        if (firebase.auth().currentUser)
          firebase
            .firestore()
            .collection("users")
            .doc(firebase.auth().currentUser.uid)
            .collection("notifications")
            .where("isViewed", "==", false)
            .onSnapshot((collection) => {
              if (collection.docs.length > 0) {
                AppConstants.setAreNotifications(true);
              }
            });
        else console.log("No user auth");
      }

      appState.current = nextAppState;
    });

    return subscription;
  }, []);

  useEffect(() => {
    let mounted = false;

    const fetchData = async () => {
      if (currentUser) {
        try {
          //Notification received when the app is in the foreground
          Notifications.addNotificationReceivedListener(
            async (notification) => {
              await firebase
                .firestore()
                .collection("users")
                .doc(currentUser.uid)
                .collection("notifications")
                .doc(notification.request.content.data.docId)
                .get()
                .then((doc) => {
                  if (doc.exists) {
                    AppConstants.setAreNotifications(true);
                  }
                });
            }
          );

          const response = await firebase
            .firestore()
            .collection("users")
            .doc(currentUser.uid)
            .get();

          if (response.exists && !mounted) {
            setIsCompletedConfiguration(
              response.data().isCompletedConfiguration
            );
            setLoadingSettings(false);
          }
        } catch (error) {
          console.log(error);
        }
      }
    };

    fetchData();

    return () => (mounted = true);
  }, [currentUser]);

  useEffect(() => {
    let mounted = false;
    const fetchData = async () => {
      if (currentUser) {
        try {
          const response = await firebase
            .firestore()
            .collection("users")
            .doc(currentUser.uid)
            .get();

          if (response.exists && !mounted) {
            setCurrentUserData(response.data());
          }
        } catch (error) {
          console.log(error);
        }
      }
    };

    fetchData();

    return () => (mounted = true);
  });

  useEffect(() => {
    if (currentUserData) {
      const isCompletedConfig = currentUserData.isCompletedConfiguration;
      setIsCompletedConfiguration(isCompletedConfig);
    }
  }, [currentUserData]);

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      if (state.type !== "unknown" && state.isInternetReachable === false);
      setIsConnected(true);

      return () => unsubscribe();
    });
  }, []);

  return (
    <NetworkProvider>
      <AuthProvider>
        {!loading && (
          <React.Fragment>
            <NavigationContainer theme={navigationTheme}>
              {!currentUser && <AuthNavigator />}
              {!loadingSettings &&
              currentUser &&
              !isCompletedConfiguration &&
              isConnected ? (
                <BasicAccountSettings />
              ) : !loadingSettings && currentUser && isConnected ? (
                <AppNavigator />
              ) : !isConnected ? (
                <OfflineScreen />
              ) : null}
            </NavigationContainer>
            <OfflineNotice />
          </React.Fragment>
        )}
      </AuthProvider>
    </NetworkProvider>
  );
}
